package com.jamoon.webservice;

import com.google.gson.JsonObject;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.Models.UserDetails;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("Login")
    Call<ResponseOfAllApis> dologin (@Field("Email") String email,@Field("Password") String password,@Field("LoginProviderId") int loginProvided,@Field("RegisterdId") String registerId);
    @FormUrlEncoded
    @POST("InsertUser")
    Call<ResponseOfAllApis> doSignUp (@Field("Email") String email, @Field("Password") String password, @Field("LoginProviderId") int loginProvided, @Field("RegisterId") String registerId);

    @FormUrlEncoded
    @POST("GetDealsForUsers")
    Call<ResponseOfAllApis>getdealListing(@Field("pageno")int pageNo,@Field("pagesize") int pageSize,
    @Field("UserId") String userId,@Field("lattitude") double latitude,@Field("longitude")double longitude,
                                          @Field("DateFilterId")int dateId,@Field("PriceId")int priceId,@Field("CategoryId") String categoryId,
                                          @Field("RatingId")int ratingId,@Field("LocationId")int locationId,@Field("IsWhistlist")int isWishList,@Field("MerchantId")int merchantId,@Field("DealType")int dealType);
    @FormUrlEncoded
    @POST("GetDealDetail")
    Call<ResponseOfAllApis>getDealDetail(@Field("DealId") int dealId ,@Field("UserId")int userId);

    @FormUrlEncoded
    @POST("InsertUserToWishlistConnection")
    Call<JsonObject>addToFav(@Field("DealId") int dealId,@Field("UserId") int userId);

    @FormUrlEncoded
    @POST("RemoveUserToWishlistConnection")
    Call<JsonObject> removeToFav(@Field("DealId")int dealId,@Field("UserId")int userId);

    @FormUrlEncoded
    @POST("GetMerchantlist")
    Call<ResponseOfAllApis> getMerchantList(@Field("lattitude")double latitude,@Field("longitude")double longitude,@Field("pageno")int pageNo,@Field("pagesize")int pageSize);

    @FormUrlEncoded
    @POST("ChangePasswordUser")
    Call<JsonObject>changePassword(@Field("OldPassword")String oldPassword,@Field("NewPassword")String newPassword,@Field("UserId")String userId);

    @Multipart
    @POST("UpdateUser")
    Call<ResponseOfAllApis> updateUserDetail(@Part("UserId") RequestBody userId, @Part("FirstName") RequestBody firstName, @Part("LastName") RequestBody lastName, @Part("ZipCode") RequestBody zipcode,
                                             @Part("Email") RequestBody email, @Part("ProfileImage") RequestBody imageName, @Part("PhoneNo") RequestBody phoneNo, @Part("ImageStatus") RequestBody imageStatus,
                                             @Part("Address1") RequestBody address1, @Part("Address2") RequestBody address2, @Part("CityName") RequestBody cityName, @Part("GenderId") RequestBody genderId,  @Part MultipartBody.Part File0);

    @FormUrlEncoded
    @POST("InsertStoreReview")
    Call<JsonObject> insertStoreReview(@Field("StoreId")int storeId,@Field("UserId")int userId,@Field("Rating")int rating,@Field("Review")String review);

    @FormUrlEncoded
    @POST("GetStoreReview")
    Call<ResponseOfAllApis>getStoreReview(@Field("StoreId")int storeId);
    @FormUrlEncoded
    @POST("InsertDealReview")
    Call<JsonObject> insertDealReview(@Field("DealId")int storeId,@Field("UserId")int userId,@Field("Rating")int rating,@Field("Review")String review);

    @FormUrlEncoded
    @POST("GetDealReview")
    Call<ResponseOfAllApis>getDealReview(@Field("DealId")int dealId);

    @FormUrlEncoded
    @POST("PurchaseDeal")
    Call<JsonObject>purchaseDeal(@Field("UserId")int userId,@Field("DealId")int dealId,@Field("Quantity")int quantity);

    @FormUrlEncoded
    @POST("InsertContactUs")
    Call<JsonObject>contactUs(@Field("UserId") int userId,@Field("Subject")String subject,@Field("Message") String message);

}
