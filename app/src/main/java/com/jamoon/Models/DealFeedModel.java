package com.jamoon.Models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DealFeedModel extends BaseObservable implements Parcelable{
    @SerializedName("DealId")
    private int dealId;
    @SerializedName("DealName")
    private String dealName;
    @SerializedName("DealPrice")
    private String dealPrice;
    @SerializedName("DealActualPrice")
    private String dealActualPrice;
    @SerializedName("DealImg1Path")
    private String dealImg1;
    @SerializedName("DealDescription")
    private String dealDecription;
    @SerializedName("DealViewCounts")
    private int viewCounts;
    @SerializedName("IsFavourite")
    private int isFav;
    @SerializedName("DealReviewsCounts")
    private int dealReviewCounts;


    @SerializedName("IsDealStarted")
    private int isDealStarted;
    @SerializedName("Remainingtime")
    private String remainingTime;
    @SerializedName("RatingId")
    private int ratingId;
    @SerializedName("NearestStoreAddress")
    private String nearestStoreAddress;
    @SerializedName("NearestStoreKm")
    private String nearestdistance;
    @SerializedName("MerchantDetails")
    private MerchantDetailModel merchantDetailModel;
    @SerializedName("StoreList")
    private ArrayList<StoreModel> storeModelArrayList;

    protected DealFeedModel(Parcel in) {
        dealId = in.readInt();
        dealName = in.readString();
        dealPrice = in.readString();
        dealActualPrice = in.readString();
        dealImg1 = in.readString();
        dealDecription = in.readString();
        isFav = in.readInt();
        isDealStarted =in.readInt();
        viewCounts=in.readInt();
        dealReviewCounts=in.readInt();
        remainingTime = in.readString();
        ratingId = in.readInt();
        nearestStoreAddress = in.readString();
        nearestdistance = in.readString();
        merchantDetailModel = in.readParcelable(MerchantDetailModel.class.getClassLoader());
        storeModelArrayList = in.createTypedArrayList(StoreModel.CREATOR);
    }

    public static final Creator<DealFeedModel> CREATOR = new Creator<DealFeedModel>() {
        @Override
        public DealFeedModel createFromParcel(Parcel in) {
            return new DealFeedModel(in);
        }

        @Override
        public DealFeedModel[] newArray(int size) {
            return new DealFeedModel[size];
        }
    };

    public ArrayList<StoreModel> getStoreModelArrayList() {
        return storeModelArrayList;
    }

    public void setStoreModelArrayList(ArrayList<StoreModel> storeModelArrayList) {
        this.storeModelArrayList = storeModelArrayList;
    }

    public String getNearestStoreAddress() {
        return nearestStoreAddress;
    }

    public void setNearestStoreAddress(String nearestStoreAddress) {
        this.nearestStoreAddress = nearestStoreAddress;
    }


    public int getDealReviewCounts() {
        return dealReviewCounts;
    }

    public void setDealReviewCounts(int dealReviewCounts) {
        this.dealReviewCounts = dealReviewCounts;
    }
    public int getIsDealStarted() {
        return isDealStarted;
    }

    public void setIsDealStarted(int isDealStarted) {
        this.isDealStarted = isDealStarted;
    }

    public String getNearestdistance() {
        return nearestdistance;
    }

    public void setNearestdistance(String nearestdistance) {
        this.nearestdistance = nearestdistance;
    }

    public int getRatingId() {
        return ratingId;
    }

    public void setRatingId(int ratingId) {
        this.ratingId = ratingId;
    }

    public int getIsFav() {
        return isFav;
    }

    public void setIsFav(int isFav) {
        this.isFav = isFav;

    }

    public String getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(String remainingTime) {
        this.remainingTime = remainingTime;
    }

    public MerchantDetailModel getMerchantDetailModel() {
        return merchantDetailModel;
    }

    public void setMerchantDetailModel(MerchantDetailModel merchantDetailModel) {
        this.merchantDetailModel = merchantDetailModel;
    }



   public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(String dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getDealActualPrice() {
        return dealActualPrice;
    }

    public void setDealActualPrice(String dealActualPrice) {
        this.dealActualPrice = dealActualPrice;
    }

    @Bindable
    public String getDealImg1() {
        return dealImg1;
    }

    public void setDealImg1(String dealImg1) {
        this.dealImg1 = dealImg1;
    }

    public String getDealDecription() {
        return dealDecription;
    }

    public void setDealDecription(String dealDecription) {
        this.dealDecription = dealDecription;
    }
    public int getViewCounts() {
        return viewCounts;
    }

    public void setViewCounts(int viewCounts) {
        this.viewCounts = viewCounts;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(dealId);
        dest.writeString(dealName);
        dest.writeString(dealPrice);
        dest.writeString(dealActualPrice);
        dest.writeString(dealImg1);
        dest.writeString(dealDecription);
        dest.writeInt(isFav);
        dest.writeInt(isDealStarted);
        dest.writeInt(viewCounts);
        dest.writeInt(dealReviewCounts);
        dest.writeString(remainingTime);
        dest.writeInt(ratingId);
        dest.writeString(nearestStoreAddress);
        dest.writeString(nearestdistance);
        dest.writeParcelable(merchantDetailModel, flags);
        dest.writeTypedList(storeModelArrayList);
    }
}
