package com.jamoon.Models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReviewListModel extends BaseObservable {
        @SerializedName("FirstName")
        private String firstName;
        @SerializedName("LastName")
        private String lastName;
        @SerializedName("UserImage")
        private String userImage;
        @SerializedName("Rating")
        private int rating;
        @SerializedName("Review")
        private String review;
        @SerializedName("ReviewLastModified")
        private String reviewLastModified;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @Bindable
        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public String getReviewLastModified() {
            return reviewLastModified;
        }

        public void setReviewLastModified(String reviewLastModified) {
            this.reviewLastModified = reviewLastModified;
        }


}
