package com.jamoon.Models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MerchantDetailModel extends BaseObservable implements Parcelable{
    @SerializedName("MerchantId")
    private int merchantId;
    @SerializedName("BusinessName")
    private String BusinessName;
    @SerializedName("BusinessImageName")
    private String businessImageName;
    @SerializedName("NearestStoreAddress")
    private String businessAddress;
    @SerializedName("TotalDeals")
    private String totalDeals;
    @SerializedName("NearestStoreKM")
    private String nearestStoredistance;
    @SerializedName("BusinessCategoryName")
    private String businessCategory;
    @SerializedName("Rating")
    int rating;
    @SerializedName("StoreList")
    private ArrayList<StoreModel> storeModelArrayList;

    protected MerchantDetailModel(Parcel in) {
        merchantId = in.readInt();
        BusinessName = in.readString();
        businessImageName = in.readString();
        businessAddress = in.readString();
        totalDeals = in.readString();
        nearestStoredistance = in.readString();
        businessCategory = in.readString();
        rating = in.readInt();
        storeModelArrayList = in.createTypedArrayList(StoreModel.CREATOR);
    }

    public static final Creator<MerchantDetailModel> CREATOR = new Creator<MerchantDetailModel>() {
        @Override
        public MerchantDetailModel createFromParcel(Parcel in) {
            return new MerchantDetailModel(in);
        }

        @Override
        public MerchantDetailModel[] newArray(int size) {
            return new MerchantDetailModel[size];
        }
    };

    public String getBusinessCategory() {
        return businessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        this.businessCategory = businessCategory;
    }





   public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getTotalDeals() {
        return totalDeals;
    }

    public void setTotalDeals(String totalDeals) {
        this.totalDeals = totalDeals;
    }

    public String getNearestStoredistance() {
        return nearestStoredistance;
    }

    public void setNearestStoredistance(String nearestStoredistance) {
        this.nearestStoredistance = nearestStoredistance;
    }
    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }
    public String getBusinessAddress() {
        return businessAddress;
    }
    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }
    @Bindable
    public String getBusinessImageName() {
        return businessImageName;
    }

    public void setBusinessImageName(String businessImageName) {
        this.businessImageName = businessImageName;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public ArrayList<StoreModel> getStoreModelArrayList() {
        return storeModelArrayList;
    }

    public void setStoreModelArrayList(ArrayList<StoreModel> storeModelArrayList) {
        this.storeModelArrayList = storeModelArrayList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(merchantId);
        dest.writeString(BusinessName);
        dest.writeString(businessImageName);
        dest.writeString(businessAddress);
        dest.writeString(totalDeals);
        dest.writeString(nearestStoredistance);
        dest.writeString(businessCategory);
        dest.writeInt(rating);
        dest.writeTypedList(storeModelArrayList);
    }
}
