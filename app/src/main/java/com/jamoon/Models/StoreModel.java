package com.jamoon.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
public class StoreModel implements Parcelable {
    @SerializedName("StoreId")
    private String storeId;
    @SerializedName("Address")
    private String address1;

    @SerializedName("Lattitude")
    private String lattitude;
    @SerializedName("Longitude")
    private String longitude;
    @SerializedName("PhoneNo")
    private String phoneNo;




    protected StoreModel(Parcel in) {
        storeId = in.readString();
        address1 = in.readString();

        lattitude = in.readString();
        longitude = in.readString();
        phoneNo=in.readString();
    }

    public static final Creator<StoreModel> CREATOR = new Creator<StoreModel>() {
        @Override
        public StoreModel createFromParcel(Parcel in) {
            return new StoreModel(in);
        }

        @Override
        public StoreModel[] newArray(int size) {
            return new StoreModel[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(storeId);
        dest.writeString(address1);

        dest.writeString(lattitude);
        dest.writeString(longitude);
        dest.writeString(phoneNo);
    }
    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhoneNo() { return phoneNo;  }

    public void setPhoneNo(String phoneNo) {   this.phoneNo = phoneNo;   }



}
