package com.jamoon.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseOfAllApis {
    @SerializedName("NextRecords")
    private int nextRecords;
    @SerializedName("UserDetails")
    private UserDetails userDetails;
    @SerializedName("DealList")
    private ArrayList<DealFeedModel>dealFeedModelArrayList;
    @SerializedName("MerchantList")
    private ArrayList<MerchantDetailModel> merchantDetailModelArrayList;
    @SerializedName("DealDeatail")
    private DealFeedModel dealFeedModel;
    @SerializedName("TotalReviews")
    private int totalReviews;
    @SerializedName("ReviewList")
    private ArrayList<ReviewListModel>reviewListModelArrayList;

    public int getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(int totalReviews) {
        this.totalReviews = totalReviews;
    }

    public ArrayList<ReviewListModel> getReviewListModelArrayList() {
        return reviewListModelArrayList;
    }

    public void setReviewListModelArrayList(ArrayList<ReviewListModel> reviewListModelArrayList) {
        this.reviewListModelArrayList = reviewListModelArrayList;
    }

    public ArrayList<MerchantDetailModel> getMerchantDetailModelArrayList() {
        return merchantDetailModelArrayList;
    }

    public void setMerchantDetailModelArrayList(ArrayList<MerchantDetailModel> merchantDetailModelArrayList) {
        this.merchantDetailModelArrayList = merchantDetailModelArrayList;
    }




    public int getNextRecords() {
        return nextRecords;
    }

    public void setNextRecords(int nextRecords) {
        this.nextRecords = nextRecords;
    }

    public DealFeedModel getDealFeedModel() {
        return dealFeedModel;
    }

    public void setDealFeedModel(DealFeedModel dealFeedModel) {
        this.dealFeedModel = dealFeedModel;
    }

    public ArrayList<DealFeedModel> getDealFeedModelArrayList() {
        return dealFeedModelArrayList;
    }

    public void setDealFeedModelArrayList(ArrayList<DealFeedModel> dealFeedModelArrayList) {
        this.dealFeedModelArrayList = dealFeedModelArrayList;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
}
