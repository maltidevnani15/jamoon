package com.jamoon.Models;

import android.os.Parcel;
import android.os.Parcelable;
public class FilterModel implements Parcelable {
    private int id;
    private boolean isChecked;
    private String name;

    public FilterModel(int id,boolean isChecked, String name) {
        this.id=id;
       this.isChecked=isChecked;
        this.name=name;
    }

    protected FilterModel(Parcel in) {
        id = in.readInt();
        isChecked = in.readByte() != 0;
        name = in.readString();
    }

    public static final Creator<FilterModel> CREATOR = new Creator<FilterModel>() {
        @Override
        public FilterModel createFromParcel(Parcel in) {
            return new FilterModel(in);
        }

        @Override
        public FilterModel[] newArray(int size) {
            return new FilterModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeByte((byte) (isChecked ? 1 : 0));
        dest.writeString(name);
    }
}
