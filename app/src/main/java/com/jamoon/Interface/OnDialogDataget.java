package com.jamoon.Interface;


import com.jamoon.Models.FilterModel;

import java.util.ArrayList;

public interface OnDialogDataget {
    public void getDialogData(int dateId, ArrayList<FilterModel>categoryList,int priceId,int locationId,int ratingId);
}
