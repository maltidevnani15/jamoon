package com.jamoon.Dialogs;

import android.app.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioGroup;

import com.jamoon.Adapter.SortByListAdapter;
import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.Models.FilterModel;
import com.jamoon.R;
import com.jamoon.databinding.LayoutDialogSortByBinding;

import java.util.ArrayList;

public class SortByDialog extends DialogFragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, OnRecyclerItemClickListener {
    LayoutDialogSortByBinding layoutDialogSortBinding;
    private SortByListAdapter sortByListAdapter;
    private ArrayList<FilterModel> sortFilterItemListCategory;
    private ArrayList<FilterModel> sortFilterItemListPrice;
    private ArrayList<FilterModel> sortFilterItemListDate;
    private ArrayList<FilterModel> sortFilterItemListLocation;


    private ArrayList<FilterModel> selectedCategoryList;
    private ArrayList<FilterModel> selectedpriceList;
    private ArrayList<FilterModel> selectedDateList;
    private ArrayList<FilterModel> selectedLocationList;
    private ArrayList<FilterModel>selectedRatingList;
    private String tag;
    private int previousPosition = -1;
    private boolean isRatingCbChecked1=false;
    private boolean isRatingCbChecked2=false;
    private boolean isRatingCbChecked3=false;
    private boolean isRatingCbChecked4=false;
    private boolean isRatingCbChecked5=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME,R.style.AppTheme_Dialog_Theme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        layoutDialogSortBinding = DataBindingUtil.inflate(inflater, R.layout.layout_dialog_sort_by, container, false);
        final View v = layoutDialogSortBinding.getRoot();
        return v;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationStyle;
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        sortFilterItemListCategory = new ArrayList<>();
        sortFilterItemListPrice = new ArrayList<>();
        sortFilterItemListLocation = new ArrayList<>();
        sortFilterItemListDate = new ArrayList<>();

        selectedCategoryList = new ArrayList<>();
        selectedpriceList = new ArrayList<>();
        selectedDateList = new ArrayList<>();
        selectedLocationList=new ArrayList<>();
        selectedRatingList=new ArrayList<>();
        addItemToDateList();
        addItemToCategoryList();
        addItemToLocationList();
        addItemToPriceList();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutDialogSortBinding.layoutDialogSortRg.setOnCheckedChangeListener(this);
        layoutDialogSortBinding.layoutDialogSortRv.setLayoutManager(layoutManager);
        layoutDialogSortBinding.layoutDialogSortIvClose.setOnClickListener(this);
        layoutDialogSortBinding.layoutDialogSortTvReset.setOnClickListener(this);
        layoutDialogSortBinding.layoutDialogSortTvApply.setOnClickListener(this);
        layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb1.setOnClickListener(this);
        layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb2.setOnClickListener(this);
        layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb3.setOnClickListener(this);
        layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb4.setOnClickListener(this);
        layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb5.setOnClickListener(this);
    }

    private void addItemToDateList() {
        sortFilterItemListDate.add(0, new FilterModel(1, false, "Today"));
        sortFilterItemListDate.add(1, new FilterModel(2, false, "Last 7 days"));
        sortFilterItemListDate.add(2, new FilterModel(3, false, "Last 30 days"));
    }
    private void addItemToCategoryList() {
        sortFilterItemListCategory.add(0, new FilterModel(1, false, "Restaurants, Cafe, Pubs Clubs, etc."));
        sortFilterItemListCategory.add(1, new FilterModel(2, false, "Spa & Salon"));
        sortFilterItemListCategory.add(2, new FilterModel(3, false, "Gyms, Diagnostic Centres, Clinic, etc."));
        sortFilterItemListCategory.add(3, new FilterModel(4, false, "Sports, Adventure Parks, Water Parks etc."));
        sortFilterItemListCategory.add(4, new FilterModel(5, false, "Hotels & Resorts."));
        sortFilterItemListCategory.add(5, new FilterModel(6, false, " Movies & Events"));
        sortFilterItemListCategory.add(6, new FilterModel(7, false, "Pest Control, Cleaning, Car Wash & Servicing etc"));
        sortFilterItemListCategory.add(7, new FilterModel(8, false, "Others (Hobbies & Learning, Instore...)"));
    }
    private void addItemToPriceList() {
        sortFilterItemListPrice.add(0, new FilterModel(1, false, "Low to high"));
        sortFilterItemListPrice.add(1, new FilterModel(2, false, "High to low"));
    }
    private void addItemToLocationList(){
        sortFilterItemListLocation.add(0,new FilterModel(1,false,"Near to me"));
    }
    @Override
    public void onResume() {
        final WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = WindowManager.LayoutParams.MATCH_PARENT;
        p.height = WindowManager.LayoutParams.WRAP_CONTENT;
        p.gravity = Gravity.BOTTOM;
        p.flags=WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        getDialog().getWindow().setAttributes(p);
        super.onResume();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_dialog_sort_iv_close:
                dismiss();
            case R.id.layout_dialog_sort_tv_reset:
                resetWholeView();
                break;
            case R.id.layout_dialog_sort_tv_apply:
                sendDataToActivity();
                break;
            case R.id.row_sort_dialog_list_cb1:
                final int id=1;
                Log.e("checked",String.valueOf(isRatingCbChecked1));
                if(isRatingCbChecked1){
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb1.setChecked(false);
                    isRatingCbChecked1=false;

                }else{
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb1.setChecked(true);
                    isRatingCbChecked1=true;
                    isRatingCbChecked2=false;
                    isRatingCbChecked3=false;
                    isRatingCbChecked4=false;
                    isRatingCbChecked5=false;
                }
                setRemoveRatingList(id,isRatingCbChecked1);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb2.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb3.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb4.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb5.setChecked(false);
                break;
            case R.id.row_sort_dialog_list_cb2:
                final int id2=2;
                Log.e("checked",String.valueOf(isRatingCbChecked2));
                if(isRatingCbChecked2){
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb2.setChecked(false);
                    isRatingCbChecked2=false;
                }else{
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb2.setChecked(true);
                    isRatingCbChecked2=true;
                    isRatingCbChecked1=false;
                    isRatingCbChecked3=false;
                    isRatingCbChecked4=false;
                    isRatingCbChecked5=false;
                }
                setRemoveRatingList(id2,isRatingCbChecked2);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb1.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb3.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb4.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb5.setChecked(false);
                break;
            case R.id.row_sort_dialog_list_cb3:
                final int id3=3;
                Log.e("checked",String.valueOf(isRatingCbChecked3));
                if(!isRatingCbChecked3){
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb3.setChecked(true);
                    isRatingCbChecked3=true;
                }else{
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb3.setChecked(false);
                    isRatingCbChecked3=false;
                    isRatingCbChecked1=false;
                    isRatingCbChecked2=false;
                    isRatingCbChecked4=false;
                    isRatingCbChecked5=false;
                }
                setRemoveRatingList(id3,isRatingCbChecked3);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb1.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb2.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb4.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb5.setChecked(false);
                break;
            case R.id.row_sort_dialog_list_cb4:
                final int id4=4;
                Log.e("checked",String.valueOf(isRatingCbChecked4));
                if(isRatingCbChecked4){
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb4.setChecked(false);
                    isRatingCbChecked4=false;
                }else{
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb4.setChecked(true);
                    isRatingCbChecked4=true;
                    isRatingCbChecked1=false;
                    isRatingCbChecked2=false;
                    isRatingCbChecked3=false;
                    isRatingCbChecked5=false;
                }
                setRemoveRatingList(id4,isRatingCbChecked4);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb1.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb2.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb3.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb5.setChecked(false);
                break;
            case R.id.row_sort_dialog_list_cb5:
                final int id5=5;
                Log.e("checked",String.valueOf(isRatingCbChecked5));
                if(isRatingCbChecked5){
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb5.setChecked(false);
                    isRatingCbChecked5=false;

                }else{
                    layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb5.setChecked(true);
                    isRatingCbChecked5=true;
                    isRatingCbChecked1=false;
                    isRatingCbChecked2=false;
                    isRatingCbChecked3=false;
                    isRatingCbChecked4=false;
                }
                setRemoveRatingList(id5,isRatingCbChecked5);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb1.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb2.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb3.setChecked(false);
                layoutDialogSortBinding.layoutDialogSortRatingView.rowSortDialogListCb4.setChecked(false);
                break;
        }

    }


    private void sendDataToActivity() {
        Intent i = new Intent();
        int dateId;
        int locationId;
        int priceId;
        int ratingId;
        if(!selectedDateList.isEmpty()){
           dateId=selectedDateList.get(0).getId();
        }else{
            dateId=0;
        }
        if(!selectedpriceList.isEmpty()){
            priceId=selectedpriceList.get(0).getId();
        }else{
            priceId=0;
        }
        if(!selectedLocationList.isEmpty()){
            locationId=selectedLocationList.get(0).getId();
        }else{
            locationId=0;
        }
        if(!selectedRatingList.isEmpty()){
            ratingId=selectedRatingList.get(0).getId();
        }else{
            ratingId=0;
        }

        i.putExtra("dateId",dateId);
        i.putExtra("priceId",priceId);
        i.putExtra("ratingId",ratingId);
        i.putExtra("locationId",locationId);
        i.putParcelableArrayListExtra("categoryList",selectedCategoryList);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
        dismiss();

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.layout_dialog_sort_rb_category:
                previousPosition=-1;
                Log.e("category", "category clicked");
                tag = "Category";
                hideRatingView();
                sortByListAdapter = new SortByListAdapter(getActivity(), sortFilterItemListCategory,selectedCategoryList, "Category");
                sortByListAdapter.setOnRecyclerItemClickListener(this);
                layoutDialogSortBinding.layoutDialogSortRv.setAdapter(sortByListAdapter);
                sortByListAdapter.notifyDataSetChanged();
                break;
            case R.id.layout_dialog_sort_rb_Ratings:
                Log.e("ratinmg", "rating clicked");
                showRatingView();
                break;
            case R.id.layout_dialog_sort_rb_date:
                previousPosition=-1;
                Log.e("date", "date clicked");
                tag = "Date";
                hideRatingView();

                sortByListAdapter = new SortByListAdapter(getActivity(), sortFilterItemListDate, selectedDateList, "Date");
                sortByListAdapter.setOnRecyclerItemClickListener(this);
                layoutDialogSortBinding.layoutDialogSortRv.setAdapter(sortByListAdapter);
                sortByListAdapter.notifyDataSetChanged();
                break;
            case R.id.layout_dialog_sort_rb_location:
                previousPosition=-1;
                Log.e("locatio", "location clicked");
                tag="Location";
                hideRatingView();

                sortByListAdapter=new SortByListAdapter(getActivity(),sortFilterItemListLocation, selectedLocationList, "Location");
                sortByListAdapter.setOnRecyclerItemClickListener(this);
                layoutDialogSortBinding.layoutDialogSortRv.setAdapter(sortByListAdapter);
                sortByListAdapter.notifyDataSetChanged();
                break;
            case R.id.layout_dialog_sort_rb_price:
                previousPosition=-1;
                Log.e("price", "price clicked");
                tag = "Price";
                hideRatingView();
                sortByListAdapter = new SortByListAdapter(getActivity(), sortFilterItemListPrice, selectedpriceList, "Price");
                sortByListAdapter.setOnRecyclerItemClickListener(this);
                layoutDialogSortBinding.layoutDialogSortRv.setAdapter(sortByListAdapter);
                sortByListAdapter.notifyDataSetChanged();
                break;
        }
    }

    public void hideRatingView() {
        layoutDialogSortBinding.layoutDialogSortRatingView.sortByDialogRgRatingView.setVisibility(View.GONE);
        layoutDialogSortBinding.layoutDialogSortRv.setVisibility(View.VISIBLE);
    }

    public void showRatingView() {
        layoutDialogSortBinding.layoutDialogSortRv.setVisibility(View.GONE);
        layoutDialogSortBinding.layoutDialogSortRatingView.sortByDialogRgRatingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(int position, View v, String tag) {
        switch (v.getId()) {
            case R.id.row_sort_dialog_list_cb:
                if (!tag.equalsIgnoreCase("Category")) {
                    setCheckBox(position);
                } else {
                    sortFilterItemListCategory.get(position).setChecked(true);
                    setRemoveCategoryData(sortFilterItemListCategory.get(position));
                }
                break;
        }
    }

    private void setRemoveCategoryData(FilterModel filterModel) {
        if (filterModel.isChecked()) {
            if (selectedCategoryList.contains(filterModel)) {
                selectedCategoryList.remove(filterModel);
            } else {
                selectedCategoryList.add(filterModel);
            }
        }
        if(selectedCategoryList.isEmpty()) {
            layoutDialogSortBinding.layoutDialogSortRbCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
           else{
            layoutDialogSortBinding.layoutDialogSortRbCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_purple_dot, 0);
        }

    }
  private void setCheckBox(int position) {
        switch (tag) {
            case "Price":
                if (previousPosition != position) {
                    FilterModel filterModel = sortFilterItemListPrice.get(position);
                    filterModel.setChecked(true);
                    sortFilterItemListPrice.set(position, filterModel);
                }
                if (previousPosition > -1) {
                FilterModel previousFilterModel = sortFilterItemListPrice.get(previousPosition);
                previousFilterModel.setChecked(false);
                    sortFilterItemListPrice.set(previousPosition, previousFilterModel);
                }
                previousPosition = position;
                if (selectedpriceList.size() >= 1){
                    selectedpriceList.clear();
                    layoutDialogSortBinding.layoutDialogSortRbPrice.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }

                if (sortFilterItemListPrice.get(position).isChecked()) {
                    selectedpriceList.add(sortFilterItemListPrice.get(position));
                    layoutDialogSortBinding.layoutDialogSortRbPrice.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_purple_dot,0);
                }
                if(selectedpriceList.isEmpty()){
                previousPosition=-1;
                }
                sortByListAdapter.notifyDataSetChanged();
                break;
            case "Date":
                if (previousPosition != position) {
                    FilterModel filterModel = sortFilterItemListDate.get(position);
                    filterModel.setChecked(true);
                    sortFilterItemListDate.set(position, filterModel);
                }
                if (previousPosition > -1) {
                    FilterModel previousFilterModel = sortFilterItemListDate.get(previousPosition);
                    previousFilterModel.setChecked(false);
                    sortFilterItemListDate.set(previousPosition, previousFilterModel);
                }
                previousPosition = position;
                if (selectedDateList.size() >= 1) {
                    selectedDateList.clear();
                    layoutDialogSortBinding.layoutDialogSortRbDate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }
                if (sortFilterItemListDate.get(position).isChecked()) {
                    selectedDateList.add(sortFilterItemListDate.get(position));
                    layoutDialogSortBinding.layoutDialogSortRbDate.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_purple_dot,0);
                }
                if(selectedDateList.isEmpty()){
                    previousPosition=-1;
                }
                sortByListAdapter.notifyDataSetChanged();
                break;
            case "Location":
                if (previousPosition != position) {
                    FilterModel filterModel = sortFilterItemListLocation.get(position);
                    filterModel.setChecked(true);
                    sortFilterItemListLocation.set(position, filterModel);

                }
                if (previousPosition > -1) {
                    FilterModel previousFilterModel = sortFilterItemListLocation.get(previousPosition);
                    previousFilterModel.setChecked(false);
                    sortFilterItemListLocation.set(previousPosition, previousFilterModel);
                }
                previousPosition = position;
                if(selectedLocationList.size()>=1){
                    selectedLocationList.clear();
                    layoutDialogSortBinding.layoutDialogSortRbLocation.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }if(sortFilterItemListLocation.get(position).isChecked()){
                        selectedLocationList.add(sortFilterItemListLocation.get(position));
                        layoutDialogSortBinding.layoutDialogSortRbLocation.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_purple_dot,0);
                    }
                if(selectedLocationList.isEmpty()){
                    previousPosition=-1;
                  }
                sortByListAdapter.notifyDataSetChanged();


        }
    }
    private void resetWholeView() {
        layoutDialogSortBinding.layoutDialogSortRg.check(layoutDialogSortBinding.layoutDialogSortRbDate.getId());
        layoutDialogSortBinding.layoutDialogSortRbCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        layoutDialogSortBinding.layoutDialogSortRbPrice.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        layoutDialogSortBinding.layoutDialogSortRbDate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        layoutDialogSortBinding.layoutDialogSortRbLocation.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        clearList();
        sortByListAdapter.notifyDataSetChanged();


    }
    private void setRemoveRatingList(int id, boolean isChecked) {
        if(selectedRatingList.size()>=1){
          selectedRatingList.clear();
            layoutDialogSortBinding.layoutDialogSortRbRatings.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            if(isChecked){
                selectedRatingList.add(0,new FilterModel(id,isChecked,null));
                layoutDialogSortBinding.layoutDialogSortRbRatings.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_purple_dot,0);
            }
        }else{
            if(isChecked){
                selectedRatingList.add(0,new FilterModel(id,isChecked,null));
                layoutDialogSortBinding.layoutDialogSortRbRatings.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_purple_dot,0);
            }
        }
    }
    private void clearList(){
        sortFilterItemListPrice.clear();
        sortFilterItemListCategory.clear();
        sortFilterItemListLocation.clear();
        sortFilterItemListDate.clear();
        selectedCategoryList.clear();
        selectedLocationList.clear();
        selectedpriceList.clear();
        selectedDateList.clear();
        addItemToPriceList();
        addItemToLocationList();
        addItemToCategoryList();
        addItemToDateList();
    }

}
