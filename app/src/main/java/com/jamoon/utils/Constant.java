package com.jamoon.utils;

import java.util.Arrays;
import java.util.Collection;

public class Constant {
    public static final String StatusCode="StatusCode";
    public static final String StatusMessage="StatusMessage";
    public static final String KEY_DATA = "data";
    public static final String USER_ID="userId";
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 10001;
    public static final int REQUEST_TO_CALL=1002;
    public static final int REQUEST_CHECK_SETTINGS = 1111;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String EXPECTED_DATE_FORMATTER = "MM/dd/yyyy";
    public static final String FCM_UNIQUE_ID="fcm_unique_id";
    public static final Collection<String> permissions = Arrays.asList("public_profile", "email");
}
