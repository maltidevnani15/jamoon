package com.jamoon.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.Models.DealFeedModel;
import com.jamoon.R;
import com.jamoon.databinding.RowDeelFeedsAdapterBinding;

import java.util.ArrayList;

public class DealFeedsAdapter extends RecyclerView.Adapter<DealFeedsAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private RowDeelFeedsAdapterBinding rowDeelFeedsAdapterBinding;
    private ArrayList<DealFeedModel> dealFeedModelArrayList;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public DealFeedsAdapter(Context context,ArrayList<DealFeedModel> dealFeedModelArrayList){
        this.context=context;
        this.dealFeedModelArrayList=dealFeedModelArrayList;
    }
    @Override
    public DealFeedsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        rowDeelFeedsAdapterBinding = DataBindingUtil.inflate(inflater, R.layout.row_deel_feeds_adapter, parent, false);
        return new ViewHolder(rowDeelFeedsAdapterBinding);
    }

    @Override
    public void onBindViewHolder(DealFeedsAdapter.ViewHolder holder, final int position) {
        holder.binding.setDealData(dealFeedModelArrayList.get(position));

    }

    @Override
    public int getItemCount() {
        return dealFeedModelArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RowDeelFeedsAdapterBinding binding;

        public ViewHolder(RowDeelFeedsAdapterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.rowDeelFeedAdapterCv.setOnClickListener(this);

        }
        public void bind(DealFeedModel dealFeedModel){
            binding.setDealData(dealFeedModel);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (onRecyclerItemClickListener != null)
                onRecyclerItemClickListener.onItemClick(getAdapterPosition(), v,"dealFeeds");

        }
    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }

}
