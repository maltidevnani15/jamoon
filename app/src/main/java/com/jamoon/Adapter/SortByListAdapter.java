package com.jamoon.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.jamoon.Interface.OnItemCheckChangeListener;
import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.Models.FilterModel;
import com.jamoon.R;
import com.jamoon.databinding.RowSortDialogListAdapterBinding;

import java.util.ArrayList;

public class SortByListAdapter extends RecyclerView.Adapter<SortByListAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<FilterModel>dataArray;
    private ArrayList<FilterModel>selectedDataArray;
    private String tag;
    private RowSortDialogListAdapterBinding rowSortDialogListAdapterBinding;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    public SortByListAdapter(Context context, ArrayList<FilterModel> dataArray, ArrayList<FilterModel> selectedDataArray, String tag) {
        this.context=context;
        this.dataArray=dataArray;
        this.selectedDataArray=selectedDataArray;
        this.tag=tag;
    }

    @Override
    public SortByListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        rowSortDialogListAdapterBinding= DataBindingUtil.inflate(inflater,R.layout.row_sort_dialog_list_adapter,parent,false);
        return new ViewHolder(rowSortDialogListAdapterBinding);
    }

    @Override
    public void onBindViewHolder(SortByListAdapter.ViewHolder holder, int position) {
        final FilterModel filterModel=dataArray.get(position);
        holder.rowSortDialogListAdapterBinding.rowSortDialogListTvName.setText(dataArray.get(position).getName());
        if(!selectedDataArray.isEmpty()){
            if(selectedDataArray.contains(filterModel)){
                holder.rowSortDialogListAdapterBinding.rowSortDialogListCb.setChecked(filterModel.isChecked());
            }else{
                holder.rowSortDialogListAdapterBinding.rowSortDialogListCb.setChecked(false);
            }
        }

    }

    @Override
    public int getItemCount() {
        return dataArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RowSortDialogListAdapterBinding rowSortDialogListAdapterBinding;
        public ViewHolder(RowSortDialogListAdapterBinding rowSortDialogListAdapterBinding) {
            super(rowSortDialogListAdapterBinding.getRoot());
            this.rowSortDialogListAdapterBinding=rowSortDialogListAdapterBinding;

            rowSortDialogListAdapterBinding.rowSortDialogListCb.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onRecyclerItemClickListener!=null)
                onRecyclerItemClickListener.onItemClick(getAdapterPosition(),v,tag);

        }

    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }
}
