package com.jamoon.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.Models.MerchantDetailModel;
import com.jamoon.R;
import com.jamoon.databinding.RowStoreAdapterBinding;

import java.util.ArrayList;

public class StoreListAdapter extends BaseAdapter{
    private Context context;
    private LayoutInflater inflater;
    private RowStoreAdapterBinding rowStoreAdapterBinding;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    private ArrayList<MerchantDetailModel>merchantArrayList;
    public StoreListAdapter(final Context context,final ArrayList<MerchantDetailModel>merchantArrayList){
        this.context=context;
        this.merchantArrayList=merchantArrayList;
    }


    @Override
    public int getCount() {
        return merchantArrayList.size();
    }

    @Override
    public MerchantDetailModel getItem(int position) {
        return merchantArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.row_store_adapter,parent,false);
            rowStoreAdapterBinding = DataBindingUtil.bind(convertView);
            holder = new ViewHolder(rowStoreAdapterBinding);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.bind(merchantArrayList.get(position));
        holder.rowStoreAdapterBinding.rowStoreAdapterCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onRecyclerItemClickListener!=null){
                    onRecyclerItemClickListener.onItemClick(position,v,"storeList");
                }
            }
        });
        return convertView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      private RowStoreAdapterBinding rowStoreAdapterBinding;

        public ViewHolder(RowStoreAdapterBinding rowStoreAdapterBinding) {
            super(rowStoreAdapterBinding.getRoot());
            this.rowStoreAdapterBinding=rowStoreAdapterBinding;

        }
        public void bind(MerchantDetailModel merchantDetailModel){
            rowStoreAdapterBinding.setMerchantData(merchantDetailModel);
            rowStoreAdapterBinding.executePendingBindings();
        }


    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }
}
