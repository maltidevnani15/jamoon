package com.jamoon.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jamoon.R;
import com.jamoon.databinding.RowMainAdapterBinding;

public class MainActivityGridAdapter extends BaseAdapter {

    private Context mContext;
    private final String[] gridViewString;
    private final int[] gridViewImageId;
    private int count=0;
   public MainActivityGridAdapter(Context context, String[] gridViewString, int[] gridViewImageId) {
        mContext = context;
        this.gridViewImageId = gridViewImageId;
        this.gridViewString = gridViewString;
    }

    @Override
    public int getCount() {
        return gridViewString.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        RowMainAdapterBinding binding;
       if (convertView == null) {
            LayoutInflater  inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.row_main_adapter,parent,false);
            binding = DataBindingUtil.bind(convertView);
            holder = new ViewHolder(binding);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.binding.rowMainTvCategoryName.setText(gridViewString[i]);
        holder.binding.rowMainAdapterIvCategory.setImageResource(gridViewImageId[i]);
        holder.binding.rowMainLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.row_main_ll:
                        count++;
                        if(count%2==0){
                            holder.binding.rowMainLl.setBackgroundResource(R.color.white);
                            holder.binding.rowMainAdapterIvDone.setVisibility(View.GONE);

                        }else{
                            holder.binding.rowMainLl.setBackgroundResource(R.color.light_white);
                            holder.binding.rowMainAdapterIvDone.setVisibility(View.VISIBLE);

                        }
                }
            }
        });
        return convertView;
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        RowMainAdapterBinding binding;
        public ViewHolder(RowMainAdapterBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
