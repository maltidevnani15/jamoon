package com.jamoon.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoon.Models.ReviewListModel;
import com.jamoon.R;
import com.jamoon.databinding.RowReviewAdapterBinding;

import java.util.ArrayList;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    RowReviewAdapterBinding rowReviewAdapterBinding;
    private ArrayList<ReviewListModel>reviewDataArrayList;
    public ReviewListAdapter(final Context context, final ArrayList<ReviewListModel>reviewDataArrayList){
        this.context=context;
        this.reviewDataArrayList=reviewDataArrayList;
    }
    @Override
    public ReviewListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      inflater=LayoutInflater.from(context);
        rowReviewAdapterBinding= DataBindingUtil.inflate(inflater, R.layout.row_review_adapter,parent,false);
        return new ViewHolder(rowReviewAdapterBinding);
    }

    @Override
    public void onBindViewHolder(ReviewListAdapter.ViewHolder holder, int position) {
        holder.setRowReviewAdapterBinding(reviewDataArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return reviewDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RowReviewAdapterBinding rowReviewAdapterBinding;
        public ViewHolder(RowReviewAdapterBinding rowReviewAdapterBinding) {
            super(rowReviewAdapterBinding.getRoot());
            this.rowReviewAdapterBinding=rowReviewAdapterBinding;
        }
        public void setRowReviewAdapterBinding(ReviewListModel reviewData){
            rowReviewAdapterBinding.setReviewData(reviewData);
            rowReviewAdapterBinding.executePendingBindings();
        }

    }
}
