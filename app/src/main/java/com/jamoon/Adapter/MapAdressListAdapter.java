package com.jamoon.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.Models.StoreModel;
import com.jamoon.R;
import com.jamoon.databinding.RowMapAddressListAdapterBinding;

import java.util.ArrayList;

public class MapAdressListAdapter extends RecyclerView.Adapter<MapAdressListAdapter.ViewHolder> {
   private RowMapAddressListAdapterBinding rowMapAddressListAdapterBinding;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<StoreModel>storeModelArrayList;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    public MapAdressListAdapter(final Context context,final ArrayList<StoreModel> storeModelArrayList){
        this.context=context;
        this.storeModelArrayList=storeModelArrayList;
    }
    @Override
    public MapAdressListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater=LayoutInflater.from(context);
        rowMapAddressListAdapterBinding= DataBindingUtil.inflate(inflater, R.layout.row_map_address_list_adapter,parent,false);
        return new ViewHolder(rowMapAddressListAdapterBinding);
    }

    @Override
    public void onBindViewHolder(MapAdressListAdapter.ViewHolder holder, int position) {
        holder.rowMapAddressListAdapterBinding.setStoreData(storeModelArrayList.get(position));

    }

    @Override
    public int getItemCount() {
        return storeModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RowMapAddressListAdapterBinding rowMapAddressListAdapterBinding;
       public ViewHolder(RowMapAddressListAdapterBinding rowMapAddressListAdapterBinding) {
            super(rowMapAddressListAdapterBinding.getRoot());
           this.rowMapAddressListAdapterBinding=rowMapAddressListAdapterBinding;
           rowMapAddressListAdapterBinding.rowMapAddressListIvCall.setOnClickListener(this);
        }
        public void setRowMapAddressListAdapterBinding(StoreModel storeModel){
            rowMapAddressListAdapterBinding.setStoreData(storeModel);
            rowMapAddressListAdapterBinding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if(onRecyclerItemClickListener!=null){
                onRecyclerItemClickListener.onItemClick(getAdapterPosition(),v,"mapAdapter");
            }
        }
    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }
}
