package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.jamoon.R;
import com.jamoon.databinding.ActivityContactUsBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;
import com.special.ResideMenu.ResideMenu;

import retrofit2.Call;

public class ContactUsActivity extends BaseActivity implements View.OnClickListener {
    private ActivityContactUsBinding activityContactUsBinding;
    private  Toolbar toolbar;
    private ImageView toolbar_right_image;
    private TextView titleText;
    @Override
    protected void initView() {
      activityContactUsBinding= DataBindingUtil.setContentView(this,R.layout.activity_contact_us);
    }

    @Override
    protected void initToolBar() {
        toolbar = (Toolbar)findViewById(R.id.activity_contact_us_tb);
        toolbar_right_image=(ImageView)toolbar.findViewById(R.id.row_toolbar_iv_done);
        toolbar_right_image.setImageResource(R.drawable.ic_search);
        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        titleText=titleTextView;
        titleText.setText("CONTACT US");
        menu = (ImageView)toolbar.findViewById(R.id.row_toolbar_iv_menu);
        menu.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_toolbar_iv_menu:
                getResideMenu().openMenu(ResideMenu.DIRECTION_LEFT);
                break;
            case R.id.activity_contact_us_tv_submit:
                validateData();
                break;
        }

    }

    private void validateData() {
        final String subject=activityContactUsBinding.activityContactUsEtSubject.getText().toString();
        final String message=activityContactUsBinding.activityContactUsEtMessage.getText().toString();
        if(!TextUtils.isEmpty(subject)){
            if(!TextUtils.isEmpty(message)){
                contactUs(subject,message);
            }else{
                Logger.showSnackbar(ContactUsActivity.this,"Enter message");
            }
        }else{
            Logger.showSnackbar(ContactUsActivity.this,"Enter subject");
        }
    }

    private void contactUs(String subject, String message) {
        Call<JsonObject>contactUs= RestClient.getInstance().getApiInterface().contactUs(Integer.parseInt(Utils.getString(this, Constant.USER_ID)),subject,message);
        contactUs.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
               Logger.toast(ContactUsActivity.this,data.get(Constant.StatusMessage).getAsString());

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }
}
