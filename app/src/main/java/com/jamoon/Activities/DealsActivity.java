package com.jamoon.Activities;

import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jamoon.Adapter.ViewPagerAdapter;

import com.jamoon.Fragments.ExclusiveFragment;
import com.jamoon.Fragments.DealFeedsFragment;
import com.jamoon.Fragments.WishListFragment;
import com.jamoon.R;
import com.jamoon.databinding.ActivityDealsBinding;
import com.special.ResideMenu.ResideMenu;

import java.util.ArrayList;

public class DealsActivity extends BaseActivity implements View.OnClickListener {
    private ActivityDealsBinding activityDealsBinding;
    private  Toolbar toolbar;
    public static DealsActivity instance;
    private ImageView toolbar_right_image;
    private TextView titleText;


    @Override
    protected void initView() {
        instance=this;
        activityDealsBinding = DataBindingUtil.setContentView(this,R.layout.activity_deals);



    }
    public static DealsActivity getInstance(){
        return instance;
    }

    public TextView getTitleText(){
        return titleTextView;
    }
    @Override
    protected void initToolBar() {
         toolbar = (Toolbar)findViewById(R.id.activity_deal_tb);
         toolbar_right_image=(ImageView)toolbar.findViewById(R.id.row_toolbar_iv_done);
         toolbar_right_image.setImageResource(R.drawable.ic_search);
         titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        titleText=titleTextView;
         menu = (ImageView)toolbar.findViewById(R.id.row_toolbar_iv_menu);
        menu.setOnClickListener(this);
        setUpViewPager();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_toolbar_iv_menu:
                getResideMenu().openMenu(ResideMenu.DIRECTION_LEFT);
                break;
         }
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    private void setUpViewPager() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            final TabLayout.Tab tab = activityDealsBinding.detailTabs.newTab();
            final View view = LayoutInflater.from(this).inflate(R.layout.layout_tab, null);
            final TextView tabTextView = (TextView) view.findViewById(R.id.tabText);
            switch (i) {
                case 0:
                    tabTextView.setText("HOT DEALS");
                    tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_hot,0,0,0);
                    fragments.add(new DealFeedsFragment(getInstance()));
                    break;
                case 1:
                    tabTextView.setText("EXCLUSIVE");
                    tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_exclusive,0,0,0);
                    fragments.add(new ExclusiveFragment(getInstance()));
                    break;
                case 2:
                    tabTextView.setText("WISHLIST");
                    tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_wishlist,0,0,0);
                    fragments.add(new WishListFragment(getInstance()));
                    break;

            }
            tab.setCustomView(view);
            activityDealsBinding.detailTabs.addTab(tab);
        }

        final ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments);
        activityDealsBinding.viewpager.setAdapter(pagerAdapter);
        activityDealsBinding.viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(activityDealsBinding.detailTabs));
        activityDealsBinding.detailTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0:
                        activityDealsBinding.viewpager.setCurrentItem(tab.getPosition());
                        titleText.setText("HOT DEALS");
                        break;
                    case 1:
                        activityDealsBinding.viewpager.setCurrentItem(tab.getPosition());
                        titleText.setText("EXCLUSIVE");
                        break;
                    case 2:
                        activityDealsBinding.viewpager.setCurrentItem(tab.getPosition());
                        titleText.setText("WISHLIST");
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
