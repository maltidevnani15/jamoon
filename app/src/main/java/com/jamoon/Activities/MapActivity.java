package com.jamoon.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jamoon.Adapter.MapAdressListAdapter;
import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.JamoonApplication;
import com.jamoon.Models.StoreModel;
import com.jamoon.R;
import com.jamoon.databinding.ActivityMapBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;

import java.util.ArrayList;
import java.util.Locale;

public class MapActivity extends BaseActivityWithoutResideMenu implements OnMapReadyCallback, OnRecyclerItemClickListener, GoogleMap.OnMapLoadedCallback {
    private ActivityMapBinding activityMapBinding;
    private GoogleMap map;
    private MapAdressListAdapter mapAdressListAdapter;
    private ArrayList<StoreModel>storeModelArrayList;
    @Override
    protected void initView() {
        activityMapBinding= DataBindingUtil.setContentView(this,R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        Intent intent=getIntent();
        storeModelArrayList=intent.getParcelableArrayListExtra("storeList");





    }

    @Override
    protected void onStart() {
        super.onStart();
        checkPhoneCallPermission();
        activityMapBinding.activityMapRv.setLayoutManager(new LinearLayoutManager(this));
        mapAdressListAdapter=new MapAdressListAdapter(this,storeModelArrayList);
        mapAdressListAdapter.setOnRecyclerItemClickListener(this);
        activityMapBinding.activityMapRv.setAdapter(mapAdressListAdapter);
    }

    @Override
    protected void initToolBar() {
        final Toolbar toolbar = (Toolbar)findViewById(R.id.activity_map_tb);
        setSupportActionBar(toolbar);
         showBackButton(R.drawable.ic_small_back);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Add a marker in Sydney and move the camera
        map.setOnMapLoadedCallback(this);

    }


    @Override
    public void onItemClick(int position, View v, String tag) {
        switch (v.getId()){
            case R.id.row_map_address_list_iv_call:
                 if (ActivityCompat.checkSelfPermission(MapActivity.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                   checkPhoneCallPermission();
                }else{
                     Intent callIntent = new Intent(Intent.ACTION_CALL);
                     callIntent.setData(Uri.parse(String.format(Locale.getDefault(), "tel:%s", getString(R.string._91) + storeModelArrayList.get(position).getPhoneNo())));
                     startActivity(callIntent);
                }


        }
    }
    private void checkPhoneCallPermission() {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE},
                        Constant.REQUEST_TO_CALL);
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constant.REQUEST_TO_CALL:
                if(grantResults[0]!=PackageManager.PERMISSION_GRANTED){
                    Logger.toast(this, "Please grant call permission from Apps setting");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onMapLoaded() {
        for(int i = 0 ; i < storeModelArrayList.size() ; i++ ) {
            createMarker(storeModelArrayList.get(i).getLattitude(), storeModelArrayList.get(i).getLongitude());
        }
    }
    protected void createMarker(String latitude, String longitude) {
        final double lat=Double.parseDouble(latitude);
        final double lang=Double.parseDouble(longitude);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(lat, lang));

        map.addMarker(new MarkerOptions().position(new LatLng(lat, lang)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
        LatLngBounds bounds = builder.build();
        int padding = 350;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 15));
        map.moveCamera(cu);
        map.animateCamera(zoom);

    }
}
