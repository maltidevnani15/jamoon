package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;

import com.jamoon.R;
import com.jamoon.databinding.ActivitySplashBinding;
import com.jamoon.fcm.MyFirebaseInstanceIDService;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;

public class SplashActivity extends BaseActivity {
    ActivitySplashBinding activitySplashBinding;
    @Override
    protected void initView() {
        activitySplashBinding = DataBindingUtil.setContentView(this,R.layout.activity_splash);
        new SplashCountDown(2000,1000).start();
    }

    @Override
    protected void initToolBar() {

    }


    private class SplashCountDown extends CountDownTimer {
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public SplashCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            if(Utils.isConnectedToInternet(SplashActivity.this)){
                final String user_id = Utils.getString(SplashActivity.this, Constant.USER_ID);
                Log.e("token",Utils.getString(SplashActivity.this,Constant.FCM_UNIQUE_ID));

                Intent intent;
                if (!TextUtils.isEmpty(user_id)) {
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, SignInActivity.class);
                }
                navigateToNextActivity(intent, true);
            }else{
                Logger.toast(SplashActivity.this,"No internet Connection");
                finish();
            }

        }

    }
    @Override
    protected void onStart() {
        super.onStart();
        if(Utils.checkPlayServices(SplashActivity.this)){
            Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent);
        }

    }

}
