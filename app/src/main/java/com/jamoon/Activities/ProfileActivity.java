package com.jamoon.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jamoon.ImageUtils.ImagePicker;
import com.jamoon.ImageUtils.ImagePickerInterface;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.Models.UserDetails;
import com.jamoon.R;
import com.jamoon.databinding.ActivityProfileBinding;
import com.jamoon.databinding.LayoutDialogChangePasswordBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class ProfileActivity extends BaseActivityWithoutResideMenu implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, ImagePickerInterface, ImagePicker.OnGetBitmapListener {
    ActivityProfileBinding activityProfileBinding;
    private UserDetails userDetails;
    private int genederType;
    private ImagePicker imagePicker;
    private Uri uri = null;
    private String imageName;
    private int imageStatus=0;
    private AlertDialog alertDialog;
    private  LayoutDialogChangePasswordBinding layoutDialogChangePwd;

    @Override
    protected void initView() {
        imagePicker=new ImagePicker(this,this);
        imagePicker.setOnGetBitmapListener(this);

        activityProfileBinding= DataBindingUtil.setContentView(this,R.layout.activity_profile);
        String userData = Utils.getString(ProfileActivity.this, Constant.KEY_DATA);
        if(!TextUtils.isEmpty(userData)){
            activityProfileBinding.setUserData(new Gson().fromJson(userData, UserDetails.class));
            activityProfileBinding.executePendingBindings();
        }
        activityProfileBinding.activityProfileIvEdit.setOnClickListener(this);
        activityProfileBinding.activityProfileEditImage.setOnClickListener(this);
        activityProfileBinding.activityProfileTvChangePwd.setOnClickListener(this);
        activityProfileBinding.activityProfileRg.setOnCheckedChangeListener(this);
        activityProfileBinding.activityProfileTvSaveChanges.setOnClickListener(this);
    }

    @Override
    protected void initToolBar() {
        final Toolbar toolbar=(Toolbar)findViewById(R.id.activity_profile_tb);
        setSupportActionBar(toolbar);
        showBackButton(R.drawable.ic_small_back);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_profile_iv_edit:
                userDetails=activityProfileBinding.getUserData();
                userDetails.setInEditMode(true);
                imageName=userDetails.getImageName();
                Log.e("imageName",imageName);
                if(userDetails.getGender()==1){
                    genederType=1;
                }else if(userDetails.getGender()==2){
                    genederType=2;
                }
                activityProfileBinding.setUserData(userDetails);
                activityProfileBinding.executePendingBindings();
                break;
            case R.id.activity_profile_tv_save_changes:
                validateData();
                break;
            case R.id.activity_profile_edit_image:
                imageStatus=1;
                openImagePicker(v);
                break;
            case R.id.activity_profile_tv_change_pwd:
                openChangePwdDialog();
                break;
            case R.id.dialog_change_pwd_tv_cancel:
                alertDialog.dismiss();
                break;
            case R.id.dialog_change_pwd_tv_save:
                validatePasswordData();
                break;

        }
    }

    private void validatePasswordData() {
        final String oldPwd=layoutDialogChangePwd.dialogChangePwdOldPwd.getText().toString();
        final String newPwd=layoutDialogChangePwd.dialogChangePwdNewPwd.getText().toString();
        final String confrmPwd=layoutDialogChangePwd.dialogChangePwdConfirmPwd.getText().toString();
        if(!TextUtils.isEmpty(oldPwd)){
            if(!TextUtils.isEmpty(newPwd)){
                if(!TextUtils.isEmpty(confrmPwd)){
                    if(newPwd.equalsIgnoreCase(confrmPwd)){
                        changePwd(oldPwd,newPwd);
                    }else{
                       Logger.showSnackbar(this,"New password and confirm password does not match");
                    }

                }else{
                    Logger.showSnackbar(this,"Enter confirm password");
                }
            }else{
                Logger.showSnackbar(this,"Enter New password");
            }
        }else{
            Logger.showSnackbar(this,"Enter old password");
        }
    }

    private void changePwd(String oldPwd, String newPwd) {
        Call<JsonObject>changePwd=RestClient.getInstance().getApiInterface().changePassword(oldPwd,newPwd,Utils.getString(this,Constant.USER_ID));
        changePwd.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                    Logger.toast(ProfileActivity.this,data.get(Constant.StatusMessage).getAsString());
                alertDialog.dismiss();


            }
        });
    }

    private void openChangePwdDialog() {
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(this);
        LayoutInflater inflater=LayoutInflater.from(this);
        layoutDialogChangePwd = DataBindingUtil.inflate(inflater, R.layout.layout_dialog_change_password, null, false);
        alertDialogBuilder.setView(layoutDialogChangePwd.getRoot());
         alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        layoutDialogChangePwd.dialogChangePwdTvCancel.setOnClickListener(this);
        layoutDialogChangePwd.dialogChangePwdTvSave.setOnClickListener(this);
    }

    private void validateData() {
        final String fn=activityProfileBinding.activityProfileTvFn.getText().toString();
        final String ln = activityProfileBinding.activityProfileTvLn.getText().toString();
        final String mail=activityProfileBinding.activityProfileTvEmail.getText().toString();
        final String phone = activityProfileBinding.activityProfileTvPhone.getText().toString();
        final String city = activityProfileBinding.activityProfileTvCity.getText().toString();
        final String address1 = activityProfileBinding.activityProfileTvAddress1.getText().toString();
        final String address2 = activityProfileBinding.activityProfileTvAddress2.getText().toString();
        final String zipCode = activityProfileBinding.activityProfileTvZipCode.getText().toString();
        if(!TextUtils.isEmpty(fn)){
            if(!TextUtils.isEmpty(ln)){
                if(!TextUtils.isEmpty(mail)&& Utils.isEmailValid(mail)){
                    if(!TextUtils.isEmpty(phone)){
                        if(genederType>0){
                            if(!TextUtils.isEmpty(city)){
                                if(!TextUtils.isEmpty(address1)){
                                    if(!TextUtils.isEmpty(address2)){
                                        if(!TextUtils.isEmpty(zipCode)){
                                            updateUserDetail(fn,ln,phone,city,address1,address2,zipCode,mail);
                                        }else{
                                            Logger.showSnackbar(this,"Enter zipCode");
                                        }
                                    }else{
                                        Logger.showSnackbar(this,"Enter address2");
                                    }
                                }else{
                                    Logger.showSnackbar(this,"Enter address1");
                                }
                            }else{
                                Logger.showSnackbar(this,"Enter city");
                            }
                        }else{
                            Logger.showSnackbar(this,"Select gender");
                        }

                    }else{
                        Logger.showSnackbar(this,"Enter phone");
                    }
                }else{
                    Logger.showSnackbar(this,"Enter a valid email");
                }

            }else{
                Logger.showSnackbar(this,"Enter last name");
            }
        }else{
            Logger.showSnackbar(this,"Enter first name");
        }
    }

    private void updateUserDetail(String fn, String ln, String phone, String city, String address1, String address2, String zipCode,String mail) {
        final File file1;
        final String mediaType = "text/plain";
        final  MultipartBody.Part profileImage;

        if(!TextUtils.isEmpty(imagePicker.getImagePath())){
            file1 = new File(imagePicker.getImagePath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file1);
            profileImage = MultipartBody.Part.createFormData("File0", file1.getName(), reqFile);
        }else{
            profileImage = MultipartBody.Part.createFormData("File0", " ");
        }
        final RequestBody userId=RequestBody.create(MediaType.parse(mediaType),Utils.getString(this,Constant.USER_ID));
        final RequestBody firstName = RequestBody.create(MediaType.parse(mediaType), fn);
        final RequestBody lastName = RequestBody.create(MediaType.parse(mediaType), ln);
        final RequestBody email=RequestBody.create(MediaType.parse(mediaType),mail);
        final RequestBody Phone = RequestBody.create(MediaType.parse(mediaType), phone);
        final RequestBody City = RequestBody.create(MediaType.parse(mediaType), city);
        final RequestBody Address1 = RequestBody.create(MediaType.parse(mediaType), address1);
        final RequestBody Address2 = RequestBody.create(MediaType.parse(mediaType), address2);
        final RequestBody ZipCode = RequestBody.create(MediaType.parse(mediaType), zipCode);
        final RequestBody Gender = RequestBody.create(MediaType.parse(mediaType), String.valueOf(genederType));
        final RequestBody ImageName= RequestBody.create(MediaType.parse(mediaType), imageName);
        final RequestBody ImageStatus=RequestBody.create(MediaType.parse(mediaType), String.valueOf(imageStatus));

        Call<ResponseOfAllApis> updateUserDetail= RestClient.getInstance().getApiInterface().updateUserDetail(userId,firstName,lastName,
                ZipCode,email,ImageName,Phone,ImageStatus,Address1,Address2,City,Gender,profileImage);
        updateUserDetail.enqueue(new RetrofitCallback<ResponseOfAllApis>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                if(data.getUserDetails()!=null){
                    Utils.storeString(ProfileActivity.this,Constant.KEY_DATA,new Gson().toJson(data.getUserDetails()));
                    Logger.toast(ProfileActivity.this,"Details Updated");
                    backPressed();
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
            }
        });

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId){
            case R.id.activity_profile_rb_male:
                genederType=1;
                break;
            case R.id.activity_profile_rb_female:
                genederType=2;
                break;

        }
    }
    public void openImagePicker(View view) {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ;
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            imagePicker.createImageChooser();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ImagePicker.GALLERY_REQUEST) {
            uri = data.getData();
            if (!TextUtils.isEmpty(uri.toString())) {
                imagePicker.onActivityResult(requestCode, uri);
            }
        }
        if (resultCode == RESULT_OK && requestCode == ImagePicker.CAMERA_REQUEST) {
            imagePicker.onActivityResult(requestCode, uri);
        }
    }
    @Override
    public void handleCamera(Intent takePictureIntent) {
        uri = Uri.fromFile(new File(imagePicker.createOrGetProfileImageDir(this), System.currentTimeMillis() + ".jpg"));
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(takePictureIntent, ImagePicker.CAMERA_REQUEST);
    }

    @Override
    public void handleGallery(Intent galleryPickerIntent) {
        startActivityForResult(galleryPickerIntent, ImagePicker.GALLERY_REQUEST);
    }
    @Override
    public void onGetBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            activityProfileBinding.activityProfileIvProfile.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onBackPressed() {
        if(userDetails!=null && userDetails.isInEditMode()){
            userDetails=activityProfileBinding.getUserData();
            userDetails.setInEditMode(false);
            activityProfileBinding.setUserData(userDetails);
            activityProfileBinding.executePendingBindings();
        }else{
            super.onBackPressed();
        }


    }
}
