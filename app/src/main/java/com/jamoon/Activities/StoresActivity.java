package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jamoon.Activities.BaseActivity;
import com.jamoon.Activities.DealsActivity;
import com.jamoon.Adapter.StoreListAdapter;
import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.JamoonApplication;
import com.jamoon.Models.MerchantDetailModel;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.R;
import com.jamoon.databinding.ActivityStoresBinding;
import com.jamoon.utils.Logger;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;
import com.special.ResideMenu.ResideMenu;

import java.util.ArrayList;

import retrofit2.Call;

public class StoresActivity extends BaseActivity implements View.OnClickListener, OnRecyclerItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    ActivityStoresBinding activityStoresBinding;
    private  Toolbar toolbar;
    private ImageView toolbar_right_image;
    private StoreListAdapter storeListAdapter;
    private ArrayList<MerchantDetailModel>merchantArrayList;
    private double latitude;
    private double langtitude;
    @Override
    protected void initView() {
        merchantArrayList=new ArrayList<>();
        activityStoresBinding= DataBindingUtil.setContentView(this, R.layout.activity_stores);
        activityStoresBinding.activityStoresSwipeRefreshLayout .setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        activityStoresBinding.activityStoresSwipeRefreshLayout.setOnRefreshListener(this);

        storeListAdapter=new StoreListAdapter(this,merchantArrayList);
        storeListAdapter.setOnRecyclerItemClickListener(this);
        activityStoresBinding.activityStoresGv.setAdapter(storeListAdapter);
        getMerchantList();
    }

    private void getMerchantList() {
        latitude= JamoonApplication.getInstance().getLatitude();
        langtitude=JamoonApplication.getInstance().getLongitude();
        Call<ResponseOfAllApis>getMerchantList= RestClient.getInstance().getApiInterface().getMerchantList(latitude,langtitude,1,10);
        getMerchantList.enqueue(new RetrofitCallback<ResponseOfAllApis>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                if(!data.getMerchantDetailModelArrayList().isEmpty()){
                    activityStoresBinding.activityStoreTvNoData.setVisibility(View.GONE);
                    merchantArrayList.trimToSize();
                    merchantArrayList.addAll(data.getMerchantDetailModelArrayList());
                    storeListAdapter.notifyDataSetChanged();
                }else{
                    merchantArrayList.clear();
                    storeListAdapter.notifyDataSetChanged();
                    activityStoresBinding.activityStoreTvNoData.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
                merchantArrayList.clear();
                storeListAdapter.notifyDataSetChanged();
                activityStoresBinding.activityStoreTvNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void initToolBar() {
        toolbar = (Toolbar)findViewById(R.id.activity_store_tb);
        toolbar_right_image=(ImageView)toolbar.findViewById(R.id.row_toolbar_iv_done);
        toolbar_right_image.setImageResource(R.drawable.ic_search);
        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        menu = (ImageView)toolbar.findViewById(R.id.row_toolbar_iv_menu);
        titleTextView.setText("STORES");
        menu.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_toolbar_iv_menu:
                getResideMenu().openMenu(ResideMenu.DIRECTION_LEFT);
                break;
        }
    }

    @Override
    public void onItemClick(int position, View v,String tag) {
        switch (v.getId()){
            case R.id.row_store_adapter_cv:
                Intent i=new Intent(this,StoreDetailActivity.class);
                Log.e("position",String.valueOf(position+1));
                i.putExtra("merchantDetail",merchantArrayList.get(position));
                navigateToNextActivity(i,false);
        }

    }

    @Override
    public void onRefresh() {
        reset();
        activityStoresBinding.activityStoresSwipeRefreshLayout.setRefreshing(false);
    }

    private void reset() {
        merchantArrayList.clear();
        getMerchantList();
    }
}
