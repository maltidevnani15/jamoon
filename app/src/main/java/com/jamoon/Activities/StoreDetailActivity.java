package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.jamoon.Models.MerchantDetailModel;
import com.jamoon.Models.StoreModel;
import com.jamoon.R;
import com.jamoon.databinding.ActivityStoreDetailBinding;

import java.util.ArrayList;

public class StoreDetailActivity extends BaseActivityWithoutResideMenu implements View.OnClickListener {
    ActivityStoreDetailBinding activityStoreDetailBinding;
    private MerchantDetailModel merchantDetailModel;
    private ArrayList<StoreModel>storeModelArrayList;

    @Override
    protected void initView() {
        storeModelArrayList=new ArrayList<>();
        activityStoreDetailBinding= DataBindingUtil.setContentView(this,R.layout.activity_store_detail);
        Bundle bundle = getIntent().getExtras();
        merchantDetailModel = bundle.getParcelable("merchantDetail");
        if(merchantDetailModel!=null){
            storeModelArrayList=merchantDetailModel.getStoreModelArrayList();
            activityStoreDetailBinding.setMerchantdata(merchantDetailModel);
            activityStoreDetailBinding.executePendingBindings();
        }
        activityStoreDetailBinding.activityStoreDetailLl.layoutStoreDetailTvDealCount.setOnClickListener(this);
        activityStoreDetailBinding.activityStoreDetailLl.storeDetailTvViewMore.setOnClickListener(this);
        activityStoreDetailBinding.activityStoreDetailLl.layoutStoreDetailTvReview.setOnClickListener(this);
    }

    @Override
    protected void initToolBar() {
        Toolbar toolbar=(Toolbar)findViewById(R.id.activity_store_detail_tb);
        setSupportActionBar(toolbar);
        showBackButton(R.drawable.ic_small_back);

    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.store_detail_tv_view_more:
                 intent=new Intent(this,MapActivity.class);
                intent.putParcelableArrayListExtra("storeList",storeModelArrayList);
                navigateToNextActivity(intent,false);
                break;
            case R.id.layout_store_detail_tv_dealCount:
                intent=new Intent(this,MerchantStoreDealsActivity.class);
                intent.putExtra("merchantId",merchantDetailModel.getMerchantId());
                navigateToNextActivity(intent,false);
                break;
            case R.id.layout_store_detail_tv_review:
                intent=new Intent(this,ReviewActivity.class);
                intent.putExtra("merchantData",merchantDetailModel);
                navigateToNextActivity(intent,false);
                break;
        }
    }
}
