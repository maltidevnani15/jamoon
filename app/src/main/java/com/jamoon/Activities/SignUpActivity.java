package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.Models.UserDetails;
import com.jamoon.R;
import com.jamoon.databinding.ActivitySignUpBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import retrofit2.Call;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
 private ActivitySignUpBinding activitySignUpBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySignUpBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up);
    }


    @Override
    protected void onStart() {
        super.onStart();
        activitySignUpBinding.activitySignUpTvSignUp.setOnClickListener(this);
        activitySignUpBinding.activitySignUpIvBack.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_signUp_tv_signUp:
                validateData();
                break;
            case R.id.activity_signUp_iv_back:
                onBackPressed();
                break;
        }
    }

    private void validateData() {
        final String email = activitySignUpBinding.activitySignUpEtEmail.getText().toString();
        final String password = activitySignUpBinding.activitySignUpEtPwd.getText().toString();
        final String confirmPassword = activitySignUpBinding.activitySignUpEtConfrmPwd.getText().toString();
        if(!TextUtils.isEmpty(email)){
            if(Utils.isEmailValid(email)){
                if(!TextUtils.isEmpty(password)){
                    if(!TextUtils.isEmpty(confirmPassword)){
                        if(password.equalsIgnoreCase(confirmPassword)){
                            doSignUp(email,password);
                        }else{
                            Logger.showSnackbar(this,"Password and confirm password does not match");
                        }
                    }else{
                        Logger.showSnackbar(this,"Enter confirm password");
                    }
                }else{
                    Logger.showSnackbar(this,"Enter password");
                }
            }else{
                Logger.showSnackbar(this,"Email is not valid");
            }
        }else{
            Logger.showSnackbar(this,"Enter email");
        }
    }

    private void doSignUp(String email, String password) {
        Call<ResponseOfAllApis> signUp = RestClient.getInstance().getApiInterface().doSignUp(email,password,1,"");
        signUp.enqueue(new RetrofitCallback<ResponseOfAllApis>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                Logger.toast(SignUpActivity.this,getString(R.string.welcome_to_jamoon));
                Utils.storeString(SignUpActivity.this, Constant.KEY_DATA,new Gson().toJson(data.getUserDetails()));
                Utils.storeString(SignUpActivity.this,Constant.USER_ID,String.valueOf(data.getUserDetails().getUserId()));
                Intent i = new Intent(SignUpActivity.this,MainActivity.class);
                  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);

            }

            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
            }
        });

    }
}
