package com.jamoon.Activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonObject;
import com.jamoon.R;
import com.jamoon.databinding.ActivityAddReviewBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import retrofit2.Call;

public class AddReviewActivity extends BaseActivityWithoutResideMenu implements View.OnClickListener {
    ActivityAddReviewBinding activityAddReviewBinding;
    private int storeId;
    private int dealId;
    @Override
    protected void initView() {
        activityAddReviewBinding= DataBindingUtil.setContentView(this,R.layout.activity_add_review);
        storeId=getIntent().getIntExtra("storeId",0);
        dealId=getIntent().getIntExtra("dealId",0);
        activityAddReviewBinding.activityAddReviewTvSubmit.setOnClickListener(this);

    }

    @Override
    protected void initToolBar() {
        Toolbar toolbar=(Toolbar)findViewById(R.id.activity_add_review_tb);
        setSupportActionBar(toolbar);
        showBackButton(R.drawable.ic_small_back);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_add_review_tv_submit:
                validateData();
                break;
        }

    }

    private void validateData() {
        final int rating=Math.round(activityAddReviewBinding.activityAddReviewRb.getRating());
        final String review=activityAddReviewBinding.activityAddReviewEtAddReview.getText().toString();

        if(rating!=0){
            if(!TextUtils.isEmpty(review)){
                addReview(rating,review);
            }else{
                Logger.showSnackbar(this,"Enter review");
            }
        }else{
            Logger.showSnackbar(this,"Add rating");
        }

    }

    private void addReview(int rating, String review) {
        Call<JsonObject> addReview;
        if(dealId!=0){
            addReview= RestClient.getInstance().getApiInterface().insertDealReview(dealId, Integer.parseInt(Utils.getString(this,Constant.USER_ID)),rating,review);
        }else{
            addReview= RestClient.getInstance().getApiInterface().insertStoreReview(storeId, Integer.parseInt(Utils.getString(this,Constant.USER_ID)),rating,review);
        }

        addReview.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.StatusCode).getAsInt()==1){
                    onBackPressed();

                }
                Logger.toast(AddReviewActivity.this,data.get(Constant.StatusMessage).getAsString());
            }
        });
    }
}
