package com.jamoon.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jamoon.Adapter.MainActivityGridAdapter;
import com.jamoon.ImageUtils.CircleTransform;
import com.jamoon.JamoonApplication;
import com.jamoon.Models.UserDetails;
import com.jamoon.R;
import com.jamoon.databinding.ActivityMainBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.special.ResideMenu.ResideMenu;
import com.squareup.picasso.Picasso;

public class MainActivity extends BaseActivity implements View.OnClickListener{
    private ActivityMainBinding activityMainBinding;



   private String[] gridViewString = {
            "Food", "Beauty $ Fashion", "Family", "Health $ Fitness", "Music", "Nature",
            "Auto Service", "Sports"} ;
    private  int[] gridViewImageId = {
            R.drawable.ic_food, R.drawable.ic_beauty, R.drawable.ic_family, R.drawable.ic_health,
            R.drawable.ic_music, R.drawable.ic_nature,
            R.drawable.ic_auto, R.drawable.ic_sports};

    @Override
    protected void initView() {
        activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
            setUpMenu();
       final  Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        slide_up.setDuration(1000);
        activityMainBinding.mainFragment.startAnimation(slide_up);
        MainActivityGridAdapter adapter = new MainActivityGridAdapter(this, gridViewString, gridViewImageId);
        activityMainBinding.activityMainGv.setAdapter(adapter);

    }


    @Override
    protected void initToolBar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.activity_main_tb);
        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        menu = (ImageView)toolbar.findViewById(R.id.row_toolbar_iv_menu);
       final ImageView iv_done = (ImageView)toolbar.findViewById(R.id.row_toolbar_iv_done);
        iv_done.setOnClickListener(this);
        setActionBarTitle("SELECT CATEGORIES");
        setSupportActionBar(toolbar);
        final  Animation slideRight = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_right);
        slideRight.setDuration(1000);
        toolbar.startAnimation(slideRight);
        menu.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_toolbar_iv_done:
                Intent i = new Intent(this,DealsActivity.class);
                navigateToNextActivity(i,true);
                break;
            case R.id.row_toolbar_iv_menu:
                getResideMenu().openMenu(ResideMenu.DIRECTION_LEFT);
                break;
        }

    }


}
