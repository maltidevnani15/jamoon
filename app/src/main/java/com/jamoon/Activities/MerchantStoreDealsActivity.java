package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.jamoon.Adapter.DealFeedsAdapter;
import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.JamoonApplication;
import com.jamoon.Models.DealFeedModel;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.R;
import com.jamoon.databinding.ActivityMerchantStoreDealsBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.EndlessRecyclerOnScrollListener;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;

public class MerchantStoreDealsActivity extends BaseActivityWithoutResideMenu implements OnRecyclerItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    ActivityMerchantStoreDealsBinding activityMerchantStoreDealsBinding;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    private DealFeedsAdapter dealFeedsAdapter;
    private ArrayList<DealFeedModel> dealFeedModelArrayList;
    private int nextPage=0;
    private int currentPage=1;
    private int merchantId;
    @Override
    protected void initView() {
        merchantId=getIntent().getIntExtra("merchantId",0);
        dealFeedModelArrayList = new ArrayList<>();

      activityMerchantStoreDealsBinding= DataBindingUtil.setContentView(this,R.layout.activity_merchant_store_deals);

        activityMerchantStoreDealsBinding.activityMerchantStoreDealsSwipeRefreshLayout
                .setColorSchemeResources(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
        activityMerchantStoreDealsBinding.activityMerchantStoreDealsRv.setLayoutManager(new LinearLayoutManager(this));
        dealFeedsAdapter = new DealFeedsAdapter(this,dealFeedModelArrayList);
        dealFeedsAdapter.setOnRecyclerItemClickListener(this);
        activityMerchantStoreDealsBinding.activityMerchantStoreDealsRv.setAdapter(dealFeedsAdapter);
        activityMerchantStoreDealsBinding.activityMerchantStoreDealsSwipeRefreshLayout.setOnRefreshListener(this);
        setLoadMoreListener();
        getMerchantStoreDeal();
    }
    @Override
    protected void initToolBar() {
        Toolbar toolbar=(Toolbar)findViewById(R.id.activity_merchant_store_deals_tb);
        setSupportActionBar(toolbar);
        showBackButton(R.drawable.ic_small_back);

    }
    private void getMerchantStoreDeal() {
        final String userId= Utils.getString(this, Constant.USER_ID);
        Call<ResponseOfAllApis>getMerchantStoreDeal= RestClient.getInstance().getApiInterface().getdealListing( currentPage,10,userId,JamoonApplication.getInstance().getLatitude(), JamoonApplication.getInstance().getLongitude(),0,0,"",0,0,0,merchantId,0);
        getMerchantStoreDeal.enqueue(new RetrofitCallback<ResponseOfAllApis>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                nextPage=data.getNextRecords();
                if(data.getDealFeedModelArrayList()!=null){
                    dealFeedModelArrayList.clear();
                    dealFeedModelArrayList.trimToSize();
                    dealFeedModelArrayList.addAll(data.getDealFeedModelArrayList());
                    dealFeedsAdapter.notifyDataSetChanged();
                    activityMerchantStoreDealsBinding.activityMerchantStoreDealsTvNoData.setVisibility(View.GONE);
                }else{
                    activityMerchantStoreDealsBinding.activityMerchantStoreDealsTvNoData.setVisibility(View.VISIBLE);
                    dealFeedModelArrayList.clear();
                    dealFeedsAdapter.notifyDataSetChanged();
                }
                if(nextPage==1){
                    endlessRecyclerOnScrollListener.setCurrent_page(currentPage+1);
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
                activityMerchantStoreDealsBinding.activityMerchantStoreDealsTvNoData.setVisibility(View.VISIBLE);
                dealFeedModelArrayList.clear();
                dealFeedsAdapter.notifyDataSetChanged();
            }
        });
    }


    private void setLoadMoreListener() {
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(new LinearLayoutManager(this)) {
            @Override
            public void onLoadMore(int current_page) {
                if (nextPage == 1) {
                    getMerchantStoreDeal();
                }
            }
        };
        activityMerchantStoreDealsBinding.activityMerchantStoreDealsRv.addOnScrollListener(endlessRecyclerOnScrollListener);
    }
    @Override
    public void onItemClick(int position, View v, String tag) {
        switch (v.getId()){
            case R.id.row_deel_feed_adapter_cv:
                Intent i = new Intent(this, DealDetailActivity.class);
                Log.e("dealId", String.valueOf(dealFeedModelArrayList.get(position).getDealId()));
                i.putExtra("dealId",dealFeedModelArrayList.get(position).getDealId());
                startActivity(i);
                break;
        }
    }

    @Override
    public void onRefresh() {
        dealFeedModelArrayList.clear();
        activityMerchantStoreDealsBinding.activityMerchantStoreDealsSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMerchantStoreDeal();
    }

}
