package com.jamoon.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jamoon.JamoonApplication;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.R;
import com.jamoon.databinding.ActivitySignInBinding;
import com.jamoon.facebook.FacebookAPI;
import com.jamoon.facebook.FacebookInterface;
import com.jamoon.facebook.Method;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{
    ActivitySignInBinding activitySignInBinding;
    private GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private String registerId="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkLocation();
        activitySignInBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_in);


        new FacebookAPI();
        FacebookAPI.getInstance().init(this);
        FacebookAPI.getInstance().setPermissions(Constant.permissions);
        GoogleSignInOptions googleSignIn = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestProfile().requestEmail().build();
        googleApiClient  = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignIn)
                .build();
    }
     @Override
    protected void onStart() {
        super.onStart();
         activitySignInBinding.activitySignInLlFb.setOnClickListener(this);
         activitySignInBinding.activitySignInLlGoogle.setOnClickListener(this);
         activitySignInBinding.activitySignInTvSignIn.setOnClickListener(this);
        activitySignInBinding.activitySigninTvSignUp.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_signIn_tv_signIn:
                validateData();
                break;
            case R.id.activity_signIn_ll_google:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.activity_signIn_ll_fb:
                onFaceBookClick();
                break;
            case R.id.activity_signin_tv_signUp:
                Intent i = new Intent(this,SignUpActivity.class);
                startActivity(i);
                break;

        }
    }
    private void onFaceBookClick() {
        FacebookAPI.getInstance().logout();
        FacebookAPI.getInstance().facebookMeRequest(this, Method.PROFILE, new FacebookInterface() {
            @Override
            public void getMyProfile(JSONObject jsonObject) throws JSONException {
                registerId = jsonObject.getString("id");
                final String email = jsonObject.getString("email");

                doSocialMediaLogin(registerId,email,2);

            }
        });
    }

    private void validateData() {
        final String email = activitySignInBinding.activitySignInEtEmail.getText().toString();
        final String password = activitySignInBinding.activitySignInEtPwd.getText().toString();

        if(!TextUtils.isEmpty(email)){
            if(Utils.isEmailValid(email)){
                if(!TextUtils.isEmpty(password)){
                   doLogin(email,password);
                }else{
                    Logger.showSnackbar(this,getString(R.string.enter_pwd));
                }
            }else{
                Logger.showSnackbar(this,getString(R.string.email_is_not_valid));
            }
        }else{
            Logger.showSnackbar(this,getString(R.string.enter_email));
        }
    }

    private void doLogin(String email, String password) {
        final ProgressDialog progressDialog = Logger.showProgressDialog(this);
        Call<ResponseOfAllApis> doLogin= RestClient.getInstance().getApiInterface().dologin(email,password,1,"");
        doLogin.enqueue(new RetrofitCallback<ResponseOfAllApis>(this, progressDialog) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                if(data.getUserDetails()!=null ){
                    Logger.toast(SignInActivity.this,getString(R.string.welcome_to_jamoon));
                    Utils.storeString(SignInActivity.this, Constant.KEY_DATA,new Gson().toJson(data.getUserDetails()));
                    Utils.storeString(SignInActivity.this,Constant.USER_ID, String.valueOf(data.getUserDetails().getUserId()));
                    Intent i = new Intent(SignInActivity.this,MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(i);
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }
    private void doSocialMediaLogin(final String id, final String email,final int type) {
        Call<ResponseOfAllApis> doSocialUserlogin= RestClient.getInstance().getApiInterface().dologin(email,"",type,id);
        doSocialUserlogin.enqueue(new RetrofitCallback<ResponseOfAllApis>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                Logger.toast(SignInActivity.this,getString(R.string.welcome_to_jamoon));
                Utils.storeString(SignInActivity.this, Constant.KEY_DATA,new Gson().toJson(data.getUserDetails()));
                Utils.storeString(SignInActivity.this,Constant.USER_ID,String.valueOf(data.getUserDetails().getUserId()));
                Intent i = new Intent(SignInActivity.this,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            }
            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
                doRegisterSocialMediaUser(id,email,type);
            }
        });
        if(type==3){
            googleSignOut();
        }
    }

    private void doRegisterSocialMediaUser(String id, String email,int type) {
        Call<ResponseOfAllApis> doSocialUserRegistration = RestClient.getInstance().getApiInterface().doSignUp(email,"",type,id);
        doSocialUserRegistration.enqueue(new RetrofitCallback<ResponseOfAllApis>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                Logger.toast(SignInActivity.this,getString(R.string.welcome_to_jamoon));
                Utils.storeString(SignInActivity.this, Constant.KEY_DATA,new Gson().toJson(data.getUserDetails()));
                Utils.storeString(SignInActivity.this,Constant.USER_ID,String.valueOf(data.getUserDetails().getUserId()));
                Intent i = new Intent(SignInActivity.this,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            }
        });
        if(type==3){
            googleSignOut();
        }
    }
    private void googleSignOut(){
        Auth.GoogleSignInApi.signOut(googleApiClient);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FacebookAPI.getInstance().getCallbackManager() != null) {
            FacebookAPI.getInstance().getCallbackManager().onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String googleuserId = acct.getId();
            Log.e("id",googleuserId);
            String googleUserEmail = acct.getEmail();
            Log.e("email",googleUserEmail);
            doSocialMediaLogin(googleuserId,googleUserEmail,3);
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));

        }
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Logger.showSnackbar(this,"Connection lost.Try again");
    }


    private void checkLocation() {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        final int coarsePermissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED && coarsePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION },
                        Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            final double latitude = JamoonApplication.getInstance().getLatitude();
            final double longitude = JamoonApplication.getInstance().getLongitude();
            if (latitude == 0 && longitude == 0) {
                showLocationEnableDialog();
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constant.REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    final double latitude = JamoonApplication.getInstance().getLatitude();
                    final double longitude = JamoonApplication.getInstance().getLongitude();
                    if (latitude == 0 && longitude == 0) {
                        showLocationEnableDialog();
                    }

                } else {
                    Logger.toast(this, "Please enable location from Apps setting");

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    public void showLocationEnableDialog() {
        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(JamoonApplication.getInstance().getLocationRequest());
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(JamoonApplication.getInstance().getGoogleApiClient(), builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        JamoonApplication.getInstance().onConnected(null);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(SignInActivity.this, Constant.REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }
}
