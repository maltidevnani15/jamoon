package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.JsonObject;
import com.jamoon.Models.DealFeedModel;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.Models.StoreModel;
import com.jamoon.R;
import com.jamoon.databinding.ActivityDealDetailBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

public class DealDetailActivity extends BaseActivityWithoutResideMenu implements  View.OnClickListener, AppBarLayout.OnOffsetChangedListener {
  private ActivityDealDetailBinding activityDealDetailBinding;
   private int dealId;
    private boolean isChatClicked=false;
    private boolean isReviewClicked=false;
    private boolean isshareClicked =false;
    private boolean isFavClicked=false;
    private DealFeedModel dealFeedModel;
    private ArrayList<StoreModel>storeModelArrayList;

    @Override
    protected void initView() {
        storeModelArrayList=new ArrayList<>();
        activityDealDetailBinding= DataBindingUtil.setContentView(this,R.layout.activity_deal_detail);
        activityDealDetailBinding.collapsingToolbar.setTitleEnabled(true);
        activityDealDetailBinding.activityDealDetailAppBar.addOnOffsetChangedListener(this);
        activityDealDetailBinding.activityDealDetailLl.dealDetailTvViewMore.setOnClickListener(this);
        activityDealDetailBinding.activityDealDetailLl.dealDetailTvShare.setOnClickListener(this);
        activityDealDetailBinding.activityDealDetailLl.dealDetailTvReviews.setOnClickListener(this);
        activityDealDetailBinding.activityDealDetailLl.dealDetailTvWishlist.setOnClickListener(this);
        setDealDetailView();


      }

    private void setDealDetailView() {
         dealId = getIntent().getIntExtra("dealId",0);
        if(dealId!=0){
            Log.e("dealId", String.valueOf(dealId));
            getDealDetail(dealId);
        }
    }

    private void getDealDetail(final int dealId) {
        Call<ResponseOfAllApis>getDealDetail= RestClient.getInstance().getApiInterface().getDealDetail(dealId,Integer.parseInt(Utils.getString(this,Constant.USER_ID)));
        getDealDetail.enqueue(new RetrofitCallback<ResponseOfAllApis>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                if(data.getDealFeedModel()!=null){
                    dealFeedModel=data.getDealFeedModel();
                    storeModelArrayList=data.getDealFeedModel().getStoreModelArrayList();
                    activityDealDetailBinding.setDealDetail(data.getDealFeedModel());
                    activityDealDetailBinding.executePendingBindings();
                    setData();
                }
            }
        });
    }

    private void setData() {
        activityDealDetailBinding.activityDealDetailLl.layoutDealDetailTvDealName.setText(Utils.getUpperCaseString(dealFeedModel.getDealName()));
        activityDealDetailBinding.activityDealDetailLl.layoutDealDetailTvBusinessName.setText(Utils.getUpperCaseString(dealFeedModel.getMerchantDetailModel().getBusinessName()));
    }

    @Override
    protected void initToolBar() {
        final Toolbar toolbar = (Toolbar)findViewById(R.id.deal_detail_tb);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        showBackButton(R.drawable.ic_small_back);
    }


    private void removeToFav() {
        final String userId= Utils.getString(this, Constant.USER_ID);
        final int userID=Integer.parseInt(userId);
        Call<JsonObject>removeFav=RestClient.getInstance().getApiInterface().removeToFav(dealId,userID);
        removeFav.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.StatusCode).getAsInt()==1){
                    activityDealDetailBinding.activityDealDetailLl.dealDetailTvWishlist.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_fav,0,0);
                    Logger.showSnackbar(DealDetailActivity.this,data.get(Constant.StatusMessage).getAsString());
                }else{
                    Logger.showSnackbar(DealDetailActivity.this,data.get(Constant.StatusMessage).getAsString());

                }

            }
        });
    }



    private void addToFav() {
        final String userId= Utils.getString(this, Constant.USER_ID);
        final int userID=Integer.parseInt(userId);
        Call<JsonObject>addToFav=RestClient.getInstance().getApiInterface().addToFav(dealId,userID);
        addToFav.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.StatusCode).getAsInt()==1){
                   activityDealDetailBinding.activityDealDetailLl.dealDetailTvWishlist.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_fav_selected,0,0);
                    Logger.showSnackbar(DealDetailActivity.this,data.get(Constant.StatusMessage).getAsString());
                }else{
                    Logger.showSnackbar(DealDetailActivity.this,data.get(Constant.StatusMessage).getAsString());

                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.deal_detail_tv_wishlist:
                if(dealFeedModel!=null && dealFeedModel.getIsFav()==1){
                    removeToFav();
                    dealFeedModel=null;
                    isFavClicked=false;
                }else{
                    if(!isFavClicked){
                        addToFav();
                        isFavClicked=true;
                    }else{
                        removeToFav();
                        isFavClicked=false;
                    }
                }
                break;
            case R.id.deal_detail_tv_chat:
                if(!isChatClicked){
                    activityDealDetailBinding.activityDealDetailLl.dealDetailTvChat.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_chat_selected,0,0);
                    isChatClicked=true;
                }else{
                    activityDealDetailBinding.activityDealDetailLl.dealDetailTvChat.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_chat,0,0);
                    isChatClicked=false;
                }

                break;
            case R.id.deal_detail_tv_reviews:
                if(!isReviewClicked){
                    activityDealDetailBinding.activityDealDetailLl.dealDetailTvReviews.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_review_selected,0,0);
                    isReviewClicked=true;
                }else{
                    activityDealDetailBinding.activityDealDetailLl.dealDetailTvReviews.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_review,0,0);
                    isReviewClicked=false;
                }
                 intent=new Intent(this,ReviewActivity.class);
                intent.putExtra("dealData",dealFeedModel);
                navigateToNextActivity(intent,false);
                break;
            case R.id.deal_detail_tv_share:
                if(!isshareClicked){
                    activityDealDetailBinding.activityDealDetailLl.dealDetailTvShare.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_share_selected,0,0);
                    isshareClicked =true;
                    shareData();
                }else{
                    activityDealDetailBinding.activityDealDetailLl.dealDetailTvShare.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_share,0,0);
                    isshareClicked =false;
                }
                break;
            case R.id.deal_detail_tv_view_more:
                intent=new Intent(this,MapActivity.class);
                intent.putParcelableArrayListExtra("storeList",storeModelArrayList);
                navigateToNextActivity(intent,false);
                break;
          }
    }

    private void shareData() {
        Uri imagePath = getLocalBitmapUri(activityDealDetailBinding.activityDealDetailHeaderIv);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sharingIntent.setType("image/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, dealFeedModel.getDealName());
        sharingIntent.putExtra(Intent.EXTRA_TEXT, dealFeedModel.getDealDecription());
        startActivity(Intent.createChooser(sharingIntent, "Share Image Using"));

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        boolean isShow = false;
        int scrollRange = -1;
        if (scrollRange == -1) {
            scrollRange = appBarLayout.getTotalScrollRange();
        }
        if (scrollRange + verticalOffset == 0) {
            activityDealDetailBinding.collapsingToolbar.setContentScrim(getResources().getDrawable(R.drawable.ic_background));
            isShow = true;
        } else if(!isShow) {
            activityDealDetailBinding.collapsingToolbar.setTitle("");
            isShow = false;
        }
    }
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
