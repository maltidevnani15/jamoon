package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.JsonObject;
import com.jamoon.Models.DealFeedModel;
import com.jamoon.R;
import com.jamoon.databinding.ActivityPurchaseBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import retrofit2.Call;

public class PurchaseActivity extends BaseActivityWithoutResideMenu implements View.OnClickListener {
    private ActivityPurchaseBinding activityPurchaseBinding;
    int count=0;
    private DealFeedModel dealFeedModel;


    @Override
    protected void initView() {
        activityPurchaseBinding= DataBindingUtil.setContentView(this,R.layout.activity_purchase);
        dealFeedModel=getIntent().getParcelableExtra("dealData") ;
        activityPurchaseBinding.setDealData(dealFeedModel);
        activityPurchaseBinding.executePendingBindings();
        activityPurchaseBinding.activityPurchaseTvQuantity.setText(String.valueOf(count));
        activityPurchaseBinding.activityPurchaseIvAdd.setOnClickListener(this);
        activityPurchaseBinding.activityPurchaseIvSubtract.setOnClickListener(this);
        activityPurchaseBinding.activityPurchasTvBuyNow.setOnClickListener(this);
    }
    @Override
    protected void initToolBar() {
        Toolbar toolbar=(Toolbar)findViewById(R.id.activity_purchase_tb);
        setSupportActionBar(toolbar);
        showBackButton(R.drawable.ic_small_back);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_purchase_iv_add:
                count++;
                addQuantity();
              break;
            case R.id.activity_purchase_iv_subtract:
                if(count<=0){
                    count=0;
                    activityPurchaseBinding.activityPurchaseTvQuantity.setText("0");
                }else{
                    count--;
                    addQuantity();
                }
                break;
            case R.id.activity_purchas_tv_buyNow:
                validateData();
        }

    }

    private void validateData() {
        if(count!=0){
           purchaseDeal();
        }else{
            Logger.showSnackbar(this,"Add atleast 1 quantity");
        }
    }

    private void purchaseDeal() {
        Call<JsonObject>purchaseDeal= RestClient.getInstance().getApiInterface().purchaseDeal(Integer.parseInt(Utils.getString(this, Constant.USER_ID)),dealFeedModel.getDealId(),count);
        purchaseDeal.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                Logger.toast(PurchaseActivity.this,data.get(Constant.StatusMessage).getAsString());
                Intent i=new Intent(PurchaseActivity.this,DealsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                navigateToNextActivity(i,true);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void addQuantity() {
        activityPurchaseBinding.activityPurchaseTvQuantity.setText(String.valueOf(count));
        activityPurchaseBinding.activityPurchaseTvTotalPrice.setText(String.valueOf(count* (int) Double.parseDouble(dealFeedModel.getDealPrice())));
    }
}
