package com.jamoon.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.jamoon.Adapter.ReviewListAdapter;
import com.jamoon.ImageUtils.CircleTransform;
import com.jamoon.Models.DealFeedModel;
import com.jamoon.Models.MerchantDetailModel;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.Models.ReviewListModel;
import com.jamoon.R;
import com.jamoon.databinding.ActivityReviewBinding;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;

public class ReviewActivity extends BaseActivityWithoutResideMenu implements View.OnClickListener {
    private  ActivityReviewBinding activityReviewBinding;
    private ReviewListAdapter reviewListAdapter;
    private ArrayList<ReviewListModel>reviewDataArrayList;
   private int storeId=0;
    private int dealId=0;
    private MerchantDetailModel merchantDetailModel;
    private DealFeedModel dealFeedModel;
    @Override
    protected void initView() {
        reviewDataArrayList=new ArrayList<>();
        activityReviewBinding= DataBindingUtil.setContentView(this,R.layout.activity_review);
        activityReviewBinding.activityReviewRv.setLayoutManager(new LinearLayoutManager(this));
        activityReviewBinding.setMerchantData(merchantDetailModel);
        reviewListAdapter = new ReviewListAdapter(this,reviewDataArrayList);
        activityReviewBinding.activityReviewRv.setAdapter(reviewListAdapter);
        Bundle bundle = getIntent().getExtras();
        merchantDetailModel = bundle.getParcelable("merchantData");
        dealFeedModel=bundle.getParcelable("dealData");
        if(merchantDetailModel!=null){
            storeId=merchantDetailModel.getMerchantId();
            setStoreView();
            getReviewList();
        }else if(dealFeedModel!=null){
            dealId=dealFeedModel.getDealId();
            setDealView();
            getReviewList();
        }
        activityReviewBinding.activityReviewTvAdd.setOnClickListener(this);
    }
    private void setDealView() {
        activityReviewBinding.activityReviewName.setText(Utils.getUpperCaseString(dealFeedModel.getDealName()));
        activityReviewBinding.activityReviewRb.setRating(dealFeedModel.getRatingId());
        activityReviewBinding.activityReviewTvCategory.setText(Utils.getUpperCaseString(dealFeedModel.getMerchantDetailModel().getBusinessName()));
        if(dealFeedModel.getDealImg1()!=null && !TextUtils.isEmpty(dealFeedModel.getDealImg1())){
            Picasso.with(this).load(dealFeedModel.getDealImg1()).transform(new CircleTransform()).fit().into(activityReviewBinding.activityReviewIv);
        }else{
            Picasso.with(this).load(R.drawable.ic_no_image_listing).transform(new CircleTransform()).fit().into(activityReviewBinding.activityReviewIv);
        }
    }
    private void setStoreView() {
        activityReviewBinding.activityReviewName.setText(Utils.getUpperCaseString(merchantDetailModel.getBusinessName()));
        activityReviewBinding.activityReviewRb.setRating(merchantDetailModel.getRating());
        activityReviewBinding.activityReviewTvCategory.setText(Utils.getUpperCaseString(merchantDetailModel.getBusinessCategory()));
        if(merchantDetailModel.getBusinessImageName()!=null && !TextUtils.isEmpty(merchantDetailModel.getBusinessImageName())){
            Picasso.with(this).load(merchantDetailModel.getBusinessImageName()).transform(new CircleTransform()).fit().into(activityReviewBinding.activityReviewIv);
        }else{
            Picasso.with(this).load(R.drawable.ic_no_image_listing).transform(new CircleTransform()).fit().into(activityReviewBinding.activityReviewIv);
        }

    }
   private void getReviewList() {
       Call<ResponseOfAllApis>getReviewList;
       if(storeId!=0){
           getReviewList= RestClient.getInstance().getApiInterface().getStoreReview(storeId);
       }else{
           getReviewList= RestClient.getInstance().getApiInterface().getDealReview(dealId);
       }
        getReviewList.enqueue(new RetrofitCallback<ResponseOfAllApis>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                if(data.getTotalReviews()!=0){
                    if(data.getReviewListModelArrayList()!=null && !data.getReviewListModelArrayList().isEmpty()){
                        reviewDataArrayList.clear();
                        reviewDataArrayList.trimToSize();
                        reviewDataArrayList.addAll(data.getReviewListModelArrayList());
                        reviewListAdapter.notifyDataSetChanged();
                    }
                    activityReviewBinding.setTotalCount(data);
                    activityReviewBinding.executePendingBindings();
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        reviewDataArrayList.clear();
        getReviewList();
    }

    @Override
    protected void initToolBar() {
        Toolbar toolbar=(Toolbar)findViewById(R.id.activity_review_tb);
        setSupportActionBar(toolbar);
        showBackButton(R.drawable.ic_small_back);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.activity_review_tv_add:
                intent=new Intent(this,AddReviewActivity.class);
                intent.putExtra("storeId",storeId);
                intent.putExtra("dealId",dealId);
                navigateToNextActivity(intent,false);
                break;

        }
    }
}
