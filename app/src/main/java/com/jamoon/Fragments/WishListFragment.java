package com.jamoon.Fragments;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoon.Activities.DealDetailActivity;
import com.jamoon.Activities.DealsActivity;
import com.jamoon.Adapter.DealFeedsAdapter;
import com.jamoon.Dialogs.SortByDialog;
import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.JamoonApplication;
import com.jamoon.Models.DealFeedModel;
import com.jamoon.Models.FilterModel;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.R;
import com.jamoon.databinding.FragmentWishlistBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.EndlessRecyclerOnScrollListener;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;

public class WishListFragment extends BaseFragment implements OnRecyclerItemClickListener, SwipeRefreshLayout.OnRefreshListener  {
    private Context context;
    private FragmentWishlistBinding fragmentWishListBinding;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    private LinearLayoutManager layoutManager;
    private DealFeedsAdapter dealFeedsAdapter;
    private ArrayList<DealFeedModel> dealFeedModelArrayList;
    private String tag;
    private int nextPage=0;
    private int currentPage=1;
    private String categoryList="";
    private int priceId=0;
    private int ratingId =0;
    private int dateId=0;
    private int locationId=0;
    private boolean isVisible;

    public WishListFragment(Context instance) {
        super();
        context=instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentWishListBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_wishlist,container,false);
        final View v = fragmentWishListBinding.getRoot();
        return v;
    }
    @Override
    protected void initToolbar() {
    }

    @Override
    protected void initView(View view) {
        layoutManager=new LinearLayoutManager(getContext());
        dealFeedModelArrayList = new ArrayList<>();
        fragmentWishListBinding.fragmentDealFeedSwipeRefreshLayout
                .setColorSchemeResources(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
        fragmentWishListBinding.fragmentWishlistRv.setLayoutManager(new LinearLayoutManager(getContext()));
        dealFeedsAdapter = new DealFeedsAdapter(getContext(),dealFeedModelArrayList);
        dealFeedsAdapter.setOnRecyclerItemClickListener(this);
        fragmentWishListBinding.fragmentWishlistRv.setAdapter(dealFeedsAdapter);

        fragmentWishListBinding.fragmentDealFeedSwipeRefreshLayout.setOnRefreshListener(this);
        setLoadMoreListener();
    }


    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){

            isVisible=true;
            getDealList(priceId,dateId, ratingId,locationId,categoryList);
        }else{
            isVisible=false;
        }
        // execute your data loading logic.
    }
    private void setLoadMoreListener() {
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (nextPage == 1) {
                    getDealList(priceId,dateId,ratingId,locationId,categoryList);
                }
            }
        };
        fragmentWishListBinding.fragmentWishlistRv.addOnScrollListener(endlessRecyclerOnScrollListener);
    }
    private void getDealList(int priceId,int dateId,int ratingId,int locationId,String categoryList) {
        final String userId= Utils.getString(context, Constant.USER_ID);
        final double latitiude= JamoonApplication.getInstance().getLatitude();
        final double longitude=JamoonApplication.getInstance().getLongitude();
        Log.e("lat",String.valueOf(latitiude));
        Call<ResponseOfAllApis> getDealListing = RestClient.getInstance().getApiInterface().getdealListing(currentPage,10,userId,latitiude,longitude,dateId,priceId,categoryList,ratingId,locationId,1,0,0);
        getDealListing.enqueue(new RetrofitCallback<ResponseOfAllApis>(context, Logger.showProgressDialog(context)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                nextPage=data.getNextRecords();
                if(data.getDealFeedModelArrayList()!=null && !data.getDealFeedModelArrayList().isEmpty()){
                    dealFeedModelArrayList.clear();
                    dealFeedModelArrayList.trimToSize();
                    dealFeedModelArrayList.addAll(data.getDealFeedModelArrayList());
                    dealFeedsAdapter.notifyDataSetChanged();
                    fragmentWishListBinding.fragmentWishListTvNoData.setVisibility(View.GONE);
                }else{
                    fragmentWishListBinding.fragmentWishListTvNoData.setVisibility(View.VISIBLE);
                    dealFeedModelArrayList.clear();
                    dealFeedsAdapter.notifyDataSetChanged();
                }
                if(nextPage==1){
                    endlessRecyclerOnScrollListener.setCurrent_page(currentPage+1);
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
                fragmentWishListBinding.fragmentWishListTvNoData.setVisibility(View.VISIBLE);
                dealFeedModelArrayList.clear();
                dealFeedsAdapter.notifyDataSetChanged();
            }
        });
    }
    @Override
    public void onItemClick(int position, View v,String tag) {
        switch (v.getId()){
            case R.id.row_deel_feed_adapter_cv:
                Intent i = new Intent(getActivity(), DealDetailActivity.class);
                Log.e("dealId", String.valueOf(dealFeedModelArrayList.get(position).getDealId()));
                i.putExtra("dealId",dealFeedModelArrayList.get(position).getDealId());
                startActivity(i);
                break;
        }
    }
     @Override
    public void onRefresh() {
        reset();
        fragmentWishListBinding.fragmentDealFeedSwipeRefreshLayout.setRefreshing(false);

    }
    private void reset() {
        if (!dealFeedModelArrayList.isEmpty())
            dealFeedModelArrayList.clear();
        fragmentWishListBinding.fragmentWishListTvNoData.setVisibility(View.VISIBLE);
        getDealList(priceId,dateId,ratingId,locationId,categoryList);
        setLoadMoreListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isVisible){
            getDealList(priceId,dateId,ratingId,locationId,categoryList);
        }
    }
}
