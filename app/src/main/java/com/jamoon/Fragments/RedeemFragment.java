package com.jamoon.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoon.Activities.DealsActivity;
import com.jamoon.R;
import com.jamoon.databinding.FragmentDealFeedsBinding;
import com.jamoon.databinding.FragmentRedeemBinding;

public class RedeemFragment extends BaseFragment {
    FragmentRedeemBinding fragmentRedeemBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentRedeemBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_redeem,container,false);
        final View v = fragmentRedeemBinding.getRoot();
        return v;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(View view) {

    }
}
