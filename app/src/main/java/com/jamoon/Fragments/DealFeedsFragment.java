package com.jamoon.Fragments;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoon.Activities.DealDetailActivity;
import com.jamoon.Activities.DealsActivity;
import com.jamoon.Adapter.DealFeedsAdapter;
import com.jamoon.Dialogs.SortByDialog;
import com.jamoon.Interface.OnRecyclerItemClickListener;
import com.jamoon.JamoonApplication;
import com.jamoon.Models.DealFeedModel;
import com.jamoon.Models.FilterModel;
import com.jamoon.Models.ResponseOfAllApis;
import com.jamoon.R;
import com.jamoon.databinding.FragmentDealFeedsBinding;
import com.jamoon.utils.Constant;
import com.jamoon.utils.EndlessRecyclerOnScrollListener;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;
import com.jamoon.webservice.RestClient;
import com.jamoon.webservice.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;


public class DealFeedsFragment extends BaseFragment implements OnRecyclerItemClickListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private FragmentDealFeedsBinding fragmentDealFeedsBinding;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    private DealFeedsAdapter dealFeedsAdapter;
    private ArrayList<DealFeedModel> dealFeedModelArrayList;
    private String tag;
    private int nextPage=0;
    private int currentPage=1;
    private String categoryList="";
    private int priceId=0;
    private int ratingId =0;
    private int dateId=0;
    private int locationId=0;
    private boolean isVisible;
    private Context context;

    public DealFeedsFragment(DealsActivity instance) {
        super();
        context=instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentDealFeedsBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_deal_feeds,container,false);
        final View v = fragmentDealFeedsBinding.getRoot();
        return v;
    }
    @Override
    protected void initToolbar() {
        DealsActivity.getInstance().getTitleText().setText("HOT DEALS");
    }

    @Override
    protected void initView(View view) {
        dealFeedModelArrayList = new ArrayList<>();
        fragmentDealFeedsBinding.fragmentDealFeedSwipeRefreshLayout
                .setColorSchemeResources(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
        fragmentDealFeedsBinding.fragmentDealFeedRv.setLayoutManager(new LinearLayoutManager(getContext()));
        dealFeedsAdapter = new DealFeedsAdapter(getContext(),dealFeedModelArrayList);
        dealFeedsAdapter.setOnRecyclerItemClickListener(this);
        fragmentDealFeedsBinding.fragmentDealFeedRv.setAdapter(dealFeedsAdapter);

        fragmentDealFeedsBinding.fragmentDealFeedFab.setOnClickListener(this);
        fragmentDealFeedsBinding.fragmentDealFeedSwipeRefreshLayout.setOnRefreshListener(this);
        setLoadMoreListener();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){

            isVisible=true;
            getDealList(priceId,dateId, ratingId,locationId,categoryList);
        }else{
            isVisible=false;
        }
    }

    private void setLoadMoreListener() {
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(new LinearLayoutManager(getContext())) {
            @Override
            public void onLoadMore(int current_page) {
                if (nextPage == 1) {
                    getDealList(priceId,dateId,ratingId,locationId,categoryList);
                }
            }
        };
        fragmentDealFeedsBinding.fragmentDealFeedRv.addOnScrollListener(endlessRecyclerOnScrollListener);
    }
    private void getDealList(int priceId,int dateId,int ratingId,int locationId,String categoryList) {
        final String userId= Utils.getString(context, Constant.USER_ID);
        final double latitiude= JamoonApplication.getInstance().getLatitude();
        final double longitude=JamoonApplication.getInstance().getLongitude();
        Log.e("lat",String.valueOf(latitiude));
        Call<ResponseOfAllApis>getDealListing = RestClient.getInstance().getApiInterface().getdealListing(currentPage,10,userId,latitiude,longitude,dateId,priceId,categoryList,ratingId,locationId,0,0,1);
        getDealListing.enqueue(new RetrofitCallback<ResponseOfAllApis>(context, Logger.showProgressDialog(context)) {
            @Override
            public void onSuccess(ResponseOfAllApis data) {
                nextPage=data.getNextRecords();
                if(data.getDealFeedModelArrayList()!=null&& !data.getDealFeedModelArrayList().isEmpty()){
                    dealFeedModelArrayList.clear();
                    dealFeedModelArrayList.trimToSize();
                    dealFeedModelArrayList.addAll(data.getDealFeedModelArrayList());
                    dealFeedsAdapter.notifyDataSetChanged();
                    fragmentDealFeedsBinding.fragmentDealFeedsTvNoData.setVisibility(View.GONE);
                }else{
                    fragmentDealFeedsBinding.fragmentDealFeedsTvNoData.setVisibility(View.VISIBLE);
                    dealFeedModelArrayList.clear();
                    dealFeedsAdapter.notifyDataSetChanged();
                }
                if(nextPage==1){
                    endlessRecyclerOnScrollListener.setCurrent_page(currentPage+1);
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApis> call, Throwable error) {
                super.onFailure(call, error);
                fragmentDealFeedsBinding.fragmentDealFeedsTvNoData.setVisibility(View.VISIBLE);
                dealFeedModelArrayList.clear();
                dealFeedsAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onItemClick(int position, View v ,String tag) {
        switch (v.getId()){
            case R.id.row_deel_feed_adapter_cv:
                Intent i = new Intent(getActivity(), DealDetailActivity.class);
                Log.e("dealId", String.valueOf(dealFeedModelArrayList.get(position).getDealId()));
                i.putExtra("dealId",dealFeedModelArrayList.get(position).getDealId());
                startActivity(i);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_deal_feed_fab:
                final SortByDialog sortByDialog= new SortByDialog();
                sortByDialog.setTargetFragment(this, 100);
                sortByDialog.show(getFragmentManager(),"DealFragment");
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            String categoryList;
            final ArrayList<FilterModel> categoryArrayList=data.getParcelableArrayListExtra("categoryList");
            if(!categoryArrayList.isEmpty()){
                final StringBuilder stringBuilder=new StringBuilder();
                for(int i=0; i<categoryArrayList.size(); i++){
                    stringBuilder.append(String.valueOf(categoryArrayList.get(i).getId()));
                    stringBuilder.append(",");
                }
                stringBuilder.setLength(stringBuilder.length()-1);
                categoryList=stringBuilder.toString();

            }else{
                categoryList="";
            }
            currentPage=1;
            priceId=data.getIntExtra("priceId",0);
            ratingId=data.getIntExtra("ratingId",0);
            dateId=data.getIntExtra("dateId",0);
            locationId=data.getIntExtra("locationId",0);
            getDealList(priceId,dateId,ratingId,locationId,categoryList);
        }
    }
    @Override
    public void onRefresh() {
        reset();
        fragmentDealFeedsBinding.fragmentDealFeedSwipeRefreshLayout.setRefreshing(false);

    }
    private void reset() {
        if (!dealFeedModelArrayList.isEmpty())
            dealFeedModelArrayList.clear();
        fragmentDealFeedsBinding.fragmentDealFeedsTvNoData.setVisibility(View.GONE);
        priceId=dateId=ratingId=locationId=0;
        categoryList="";
        getDealList(priceId,dateId,ratingId,locationId,categoryList);
        setLoadMoreListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isVisible){
            getDealList(priceId,dateId,ratingId,locationId,categoryList);
        }

    }
}
