package com.jamoon.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Logger;
import com.jamoon.utils.Utils;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class FCMRegistrationIntentService extends IntentService {

    public FCMRegistrationIntentService() {
        super("GCMRegistrationIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.FCM_UNIQUE_ID, refreshedToken);
        Log.e("token",refreshedToken);
    }
}
