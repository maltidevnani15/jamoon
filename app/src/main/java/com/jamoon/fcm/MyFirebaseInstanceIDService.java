package com.jamoon.fcm;

import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.jamoon.utils.Constant;
import com.jamoon.utils.Utils;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
     @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.FCM_UNIQUE_ID,refreshedToken);
        Log.e("deviceToken",refreshedToken);
    }
}
