package com.jamoon.ImageUtils;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.CropSquareTransformation;

public class ImageBindingAdapter {
    @BindingAdapter({"bind:dealImg1", "bind:error"})
    public static void loadImage1One(ImageView imageView, String url , Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }
    }
    @BindingAdapter({"bind:businessImageName","bind:error"})
    public static void loadBusinessImage(ImageView imageView,String url,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).transform(new CircleTransform()).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }
    }
    @BindingAdapter({"bind:profileImage","bind:error"})
    public static void loadProfileImage(ImageView imageView,String url,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }
    }
    @BindingAdapter({"bind:userImage","bind:error"})
    public static void loadReviewUserImage(ImageView imageView,String url,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).transform(new CircleTransform()).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }
    }
    @BindingAdapter({"bind:businessImageName","bind:errorretangle"})
    public static void loadBusinessImageRectangleView(ImageView imageView,String url,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }
    }


}
